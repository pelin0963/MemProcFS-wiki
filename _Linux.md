MemProcFS on Linux
=============================================

**MemProcFS is supported on Linux with some limitations ([see below](#Limitations)). However; MemProcFS will only analyze Windows memory images - not Linux memory!**

Pre-built Linux binaries exist for x64 and aarch64 (RPi4). MemProcFS is dependent on the following packages: `sudo apt-get install libusb-1.0 fuse lz4`

It's possible to use the MemProcFS either as a FUSE file system or via its C/C++ or Python API.

The Python version of MemProcFS requires Python 3.6+ and is available on Python pip. Please install with: `pip install memprocfs` or `pip3 install memprocfs`. Make the dependencies are covered before installing: `sudo apt-get install make gcc pkg-config libusb-1.0 libusb-1.0-0-dev fuse libfuse-dev lz4 liblz4-dev`



## Building:
MemProcFS is dependent on packages, before building please do a: `sudo apt-get install make gcc pkg-config libusb-1.0 libusb-1.0-0-dev fuse libfuse-dev lz4 liblz4-dev`

MemProcFS is also available on the [LeechCore library](https://github.com/ufrisk/LeechCore). Clone leechcore and place it alongside MemProcFS. First build LeechCore. Then build MemProcFS vmm.so by typing `make` in the` MemProcFS/vmm` directory. Then build the MemProcFS FUSE wrapper by typing `make` in the` MemProcFS/memprocfs` directory.

#### Example Build Commands:
```bash
~$  sudo apt-get install make gcc pkg-config libusb-1.0 libusb-1.0-0-dev fuse libfuse-dev lz4 liblz4-dev
~$  mkdir build
~$  cd build
~/build$  git clone https://github.com/ufrisk/LeechCore
~/build$  git clone https://github.com/ufrisk/MemProcFS
~/build$  cd LeechCore/leechcore
~/build/LeechCore/leechcore$  make
~/build/LeechCore/leechcore$  cd ../../MemProcFS/vmm
~/build/MemProcFS/vmm$  make
~/build/MemProcFS/vmm$  cd ../memprocfs
~/build/MemProcFS/memprocfs$  make
~/build/MemProcFS/memprocfs$  cd ../files
### NOTE! before running memprocfs it's recommended to copy the file 'info.db' from the latest binary
### release at https://github.com/ufrisk/MemProcFS/releases/latest and put it alongside memprocfs binary.
### info.db is an sqlite database which contains common type and symbol offsets required for some tasks.
~/build/MemProcFS/files$  ./memprocfs -device <your_dumpfile_or_device> -mount <your_full_mount_point>
```

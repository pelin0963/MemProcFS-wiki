## The `token` per-process directory

The directory _token_ exists as a sub-directory in each process directory.

The _token_ directory contains **user information** related to the process and other information extracted from the process _Token_.

The files and directories and their contents are listed below:

| File           | Description                                |
| -------------- | ------------------------------------------ |
| integrity.txt  | Process integrity level.                   |
| luid.txt       | User Logon ID.                             |
| privileges.txt | List of process privileges.                |
| session.txt    | The session ID of the process.             |
| sid.txt        | The User SID.                              |
| sid-all.txt    | All SIDs in the primary process token.     |
| user.txt       | The Username                               |

The directory exists only on Windows.



### File: privileges.txt

The file privileges.txt contains information about process privileges. The meaning of the different columns are as follows:

#### Flags: 
`e` Process privilege is **Enabled**.<br>
`p` Process privilege is **Present**.<br>
`d` Process privilege is **Enabled by Default**.<br>

```
   # Flags Privilege Name                           
--------------------------------                           
0003   -p-  SeAssignPrimaryTokenPrivilege
0004   --d  SeLockMemoryPrivilege
0007   epd  SeTcbPrivilege
...
```

### Example

The example below shows the sid, username and process privileges of a process.

[[resources/proc_token.png]]

### For Developers
The _token_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_proc_token.c_ in the _vmm_ project.

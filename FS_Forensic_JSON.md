## The `forensic/json` directory

The directory _forensic/json_ exists as a sub-directory to the file system root.

The directory is hidden by default. It will appear once forensic mode has been started and processing is completed.

The directory contains json files optimized for Elasticsearch importation as well as a powershell import script.

The files in the **forensic/json** directory are listed in the table below:
| File                           | Description                                               |
| ------------------------------ | --------------------------------------------------------- |
| elastic_import.ps1             | Elasticsearch import script.                              |
| elastic_import_unauth.ps1      | Elasticsearch import script (localhost unauth instance).  |
| general.json                   | General information. Lots of various info.                |
| registry.json                  | Registry information.                                     |
| timeline.json                  | Timeline information.                                     |

Files in the _forensic/json_ directory are read-only.

An introduction demo is available on YouTube.

<a href="https://www.youtube.com/watch?v=JcIlowlrvyI" alt="Demo of MemProcFS Python API" target="_new"><img src="http://img.youtube.com/vi/JcIlowlrvyI/0.jpg" height="350"/>

## Elasticsearch integration:

The MemProcFS JSON files are optimized for Elasticsearch importation. By the default the import script will create required indexes and import some initial dashboards. The import script will only work together with a non-authenticated Elasticsearch instance running at localhost; but it should be possible to adapt to your own Elasticsearch instance.

The JSON files will be imported into three index patterns - `mp_general`, `mp_registry` and `mp_timeline`.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_index_pattern.png"/>

## General JSON:

The index pattern `mp_general` contains different types - which are listed below. In addition to this every record contains the system id in the `sys` field.

<details><summary>type: systeminformation</summary>

System Information. Only one entry per system. `desc`: computername, `desc2`: detailed information such as time zones and boot time.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_systeminformation.png"/>
</details>



<details><summary>type: bitlocker</summary>

Bitlocker keys. `obj`: key address, `desc`: encryption type, `desc2`: dislocker unlock key.
</details>



<details><summary>type: certificate</summary>

Certificates. `desc`: certificate issuer, `desc2`: store, thumbprint and issuer.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_certificate.png"/>
</details>



<details><summary>type: device</summary>

Device information. `obj`: device object, `num`: device tree depth, `addr`: attached device object, `addr2`: driver object, `desc`: name, `desc2`: driver name and extra info (such as volume name).
</details>



<details><summary>type: driver</summary>

Driver information. `obj`: driver object, `addr`: driver module to/from address, `desc`: name, `desc2`: service name and path.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_driver.png"/>
</details>



<details><summary>type: evil</summary>

Find Evil information.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_evil.png"/>
</details>



<details><summary>type: handle</summary>

Handles. `obj`: handle object, `hex`: handle id, `desc`: handle type, `desc2`: detailed handle-dependent info.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_handle.png"/>
</details>



<details><summary>type: heap</summary>

Heap information. `size`: heap size, `addr`: heap address.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_heap.png"/>
</details>



<details><summary>type: kobj</summary>

Kernel Object Manager Object. `obj`: object address. `desc`: type, `desc2`: path/name.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_kobj.png"/>
</details>



<details><summary>type: memorymap</summary>

Physical Memory Map. `size`: region byte size. `addr(2)`: region address (base-top).
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_memorymap.png"/>
</details>



<details><summary>type: module</summary>

Loaded Modules (DLLs, EXEs). `size`: module size in memory. `addr(2)`: module address range (base-top), `desc`: name.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_module.png"/>
</details>



<details><summary>type: module-codeview</summary>

Debug and PDB information. `desc`: module, `desc2`: age, guid and pdb name/path.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_codeview.png"/>
</details>



<details><summary>type: module-versioninfo</summary>

Module version information. `desc`: module, `desc2`: CompanyName, FileDescription, FileVersion, InternalName, LegalCopyright, OriginalFilename, ProductName, ProductVersion.
</details>



<details><summary>type: net</summary>

Network connections.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_net.png"/>
</details>



<details><summary>type: process</summary>

Process information. `obj`: object address., `hex`: exe base address in memory. `desc`: process kernel path, `desc2`: flags, user, user-mode path, command line, create-time.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_process.png"/>
</details>



<details><summary>type: pte</summary>

Page Table Entry (PTE) information. `size`: range size (in bytes), `addr(2)`: address range. `desc`: flags srwx, `desc2`: tag.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_pte.png"/>
</details>



<details><summary>type: service</summary>

Service Manager Information. `obj`: service address in services.exe, `addr(2)`: address range. `desc`: name, `desc2`: start, state, type, image.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_service.png"/>
</details>



<details><summary>type: shtask</summary>

Scheduled tasks. `desc`: name, `desc2`: detailed info.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_shtask.png"/>
</details>



<details><summary>type: thread</summary>

Thread Information.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_thread.png"/>
</details>



<details><summary>type: unloadedmodule</summary>

Unloaded Modules.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_unloadedmodule.png"/>
</details>



<details><summary>type: vad</summary>

Information about Virtual Address Descriptors (VADs).
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/fc/json_vad.png"/>
</details>


<details><summary>type: virtualmachine</summary>

Device information. `obj`: vm object address, `hex`: partition id, `addr`: max guest physical memory address, `desc`: name, `desc2`: active/type/osbuild.
</details>

## Timeline JSON:
For information about the timeline please check out the demo video and the [forensic timeline](https://github.com/ufrisk/MemProcFS/wiki/FS_Forensic_Timeline) information.

## Registry JSON:
The registry JSON contains two types, one for registry `key` and one for registry `value`.


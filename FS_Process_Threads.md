## The `threads` per-process directory

The directory _threads_ exists as a sub-directory in each process directory.

The _threads_ directory contains one sub-directory for each thread named after its Thread ID (TID) and the _threads.txt_ informational file.

Each thread sub-directory contains an _info.txt_ file and files for the ETHREAD kernel struct, the kernel stack, the user mode stack and the thread environment block. Please note that not all files are guaranteed to exist - for kernel exclusive threads the user-mode kernel thread will be missing.

Each module directory contains a number of informational files and directories related to the loaded module. The files and directories and their contents are listed below:

| File               | Description                                                                   |
| ------------------ | ----------------------------------------------------------------------------- |
| __threads.txt__    | Summary of threads related to the current process.                            |
| [tid]/__info.txt__ | Information about the current thread including its user and kernel mode stack, CPU instruction and stack pointers and additional information. |
| [tid]/__ethread__  | The ETHREAD kernel structure.                                                 |
| [tid]/__kstack__   | The kernel mode thread stack.                                                 |
| [tid]/__stack__    | The user mode thread stack.                                                   |
| [tid]/__teb__      | The thread environment block page.                                            |
| [tid]/__impersonation__ | Impersonation [token](https://github.com/ufrisk/MemProcFS-dev/wiki/FS_Process_Token) (if existing). |

Files _threads.txt_ and _info.txt_ are read only. Other files in the individual thread sub-directories are writable if a write-capable memory acquisition device is used.



### File: threads.txt

The file _threads.txt_ contains information about threads related to the process. The meaning of the different columns are as follows:

```
   #    PID     TID          ETHREAD Status     WaitReason           Prio      ExitSt     StartAddress   InstructionPtr                 TEB          StackBase           StackPtr         StackLimit  CreateTime                 ExitTime                
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------                
0000   2788     404 ffffd00feda95080 5 Waiting  6 UserRequest        0  8  9        0     7ffeff16ce50                0 --           a18000 :          82e0000 >                0 >          82d2000 [2020-04-05 23:51:12 UTC ::                     ***]
0001   2788    1120 ffffd00fedca5080 5 Waiting  6 UserRequest        0  9  b        0     7ffeff16ce50     7ffefcec9a84 --           be6000 :          2f70000 >          2f6fc28 >          2f62000 [2020-04-05 23:51:10 UTC ::                     ***]
0002   2788    1176 ffffd00fee71d080 5 Waiting 15 WrQueue            0  8  8        0 ffffe0082ad6deb0                0 --           bea000 :          3070000 >                0 >          3062000 [2020-04-05 23:51:10 UTC ::                     ***]
0003   2788    1228 ffffd00fe999e080 5 Waiting  6 UserRequest        0  8  9        0     7ffeff16ce50                0 --           a44000 :          b990000 >                0 >          b982000 [2020-04-05 23:51:15 UTC ::                     ***]
0004   2788    2044 ffffd00fed45e080 5 Waiting  6 UserRequest        0  8  9        0     7ffeff16ce50                0 --           a9a000 :          6ed0000 >                0 >          6ec2000 [2020-04-05 23:53:36 UTC ::                     ***]
0005   2788    2332 ffffd00fed775080 5 Waiting  6 UserRequest        0  8  a        0 ffffe0082798b3a0                0 --           a06000 :          5de0000 >                0 >          5dd2000 [2020-04-05 23:51:11 UTC ::                     ***]
0006   2788    2504 ffffd00fed490040 4 Term    22 WrTerminated       0  8  9        0     7ffeff16ce50                0 --                0 :                0 >                0 >                0 [2020-04-05 23:51:14 UTC :: 2020-04-05 23:57:47 UTC]


```

### Example

The example below shows the files in the _threads/5372_ sub-directory of the _mspaint.exe_ process where 5372 is a thread id (TID).

[[resources/proc_threads.png]]

### For Developers
The _threads_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_proc_thread.c_ in the _vmm_ project.
## The `modules` per-process directory

The directory _modules_ exists as a sub-directory in each process directory.

The _modules_ directory contains one sub-directory for each loaded module (.DLLs and EXEs). It also contains the following files:

| File                    | Description                                                                  |
| ----------------------- | ---------------------------------------------------------------------------- |
| modules.txt             | Overview of loaded modules.                                                  |
| modules-v.txt           | Verbose overview of loaded modules.                                          |
| modules-versioninfo.txt | Module version information.                                                  |
| unloaded_modules.txt    | Information about unloaded modules.                                          |

For _kernel_ "processes" such as the System process in Windows (PID 4) the directory will contain loaded drivers (.SYS-files and .DLLs)

Each module directory contains a number of informational files and directories related to the loaded module. The files and directories and their contents are listed below:

| File             | Description                                                                   |
| ---------------- | ----------------------------------------------------------------------------- |
| base.txt         | Base virtual address of the module.                                           |
| debuginfo.txt    | PDB debug information from the debug directory.                               |
| directories.txt  | Information about the 16 data directories in the PE/MZ header.                |
| entry.txt        | Entry point virtual address of the module.                                    |
| export.txt       | Functions exported by the module.                                             |
| import.txt       | Functions imported by the module and their corresponding modules.             |
| pefile.dll       | Best-effort reconstructed module, .exe/.dll/.sys, file from memory fragments. |
| sections.txt     | Sections of the module.                                                       |
| size.txt         | Size of the module.                                                           |
| versioninfo.txt  | Module version information.                                                   |
| **directoriesd** | **Directory containing a file for each of the 16 data directories.**          |
| **sectionsd**    | **Directory containing a file for each section.**                             |

Files in the individual module directories are read-only. pefile.dll and files in the **directoriesd** and **sectionsd** sub-directories are writable if a write-capable memory acquisition device is used.

### File: export.txt

The file _export.txt_ contains information about exported functions. The meaning of the columns are as follows:
```
   # Ordinal Offset        Address Name                       ForwardedFunction
===============================================================================
0000  1000   39290 00007ff8892e9290 ---
0001  1001   2e380 00007ff8892de380 I_ScGetCurrentGroupStateW
0002  1002       0 0000000000000000 A_SHAFinal                NTDLL.A_SHAFinal
0003  1003       0 0000000000000000 A_SHAInit                 NTDLL.A_SHAInit
0004  1004       0 0000000000000000 A_SHAUpdate               NTDLL.A_SHAUpdate
0005  1005   43710 00007ff8892f3710 AbortSystemShutdownA
0006  1006   437b0 00007ff8892f37b0 AbortSystemShutdownW
...
```

### File: sections.txt

The file _sections.txt_ contains information about the module PE sections. The meaning of the columns are as follows:

```
#   name  memory_address   base_offset   size page file_offset   size
=====================================================================
00 .text  00007ff7959a1000 00001000  0002e338 r-x  00000400  0002e400
...
05 .rsrc  00007ff7959fb000 0005b000  000084f8 r--  0003b600  00008600
06 .reloc 00007ff795a04000 00064000  00000308 r--  00043c00  00000400
```

### Example

The example below shows the files in the _modules/ADVAPI.dll_ sub-directory of the _connector_ctrl_ process. All files are related to the _ADVAPI.dll_ module which is loaded into the connector_ctrl process.

Shown in Notepad are the sections of the .DLL. While Ubuntu shows the entry, size, exported functions and the three first imported functions.

[[resources/proc_modules.png]]

### For Developers
The _modules_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_proc_ldrmodules.c_ in the _vmm_ project.

## The `sys/services` directory

The directory _sys/services_ exists as a sub-directory to the file system root.

The directory and its sub-directories contains information about services extracted from the service control manager (SCM).

The files in the **sys/services** directory are listed in the table below:
| File                           | Description                                               |
| ------------------------------ | --------------------------------------------------------- |
| __services.txt__               | Summary information about all services listed by ordinal. |
| by-id/[id]/registry/           | Service registry key.                                     |
| by-id/[id]/__svcinfo.txt__     | Detailed information about each service.                  |
| by-name/[name]/registry/       | Service registry key.                                     |
| by-name/[name]/__svcinfo.txt__ | Detailed information about each service.                  |

Files in the _sys/services_ directory and sub-directories are read-only except for binary registry key/values.

### File: services.txt

The file _services.txt_ contains summary information about the services. The meaning of the different columns are as follows:

```
#       PID START_TP     STATE      TYPE TYPE    RECORD_ADDR  NAME / DISPLAYNAME                              USER           IMAGE-PATH                            OBJECT-NAME
==============================================================================================================================================================================
...
0034   1332 AUTO_START   RUNNING    PROC SHR     000000b76a20 BFE / Base Filtering Engine                     LOCAL SERVICE  %SystemRoot%\System32\bfe.dll      :: C:\Windows\system32\svchost.exe -k LocalServiceNoNetwork
0035    112 AUTO_START   RUNNING    PROC SHR     000000b74cc0 BITS / Background Intelligent Transfer Service  SYSTEM         %SystemRoot%\System32\qmgr.dll     :: C:\Windows\system32\svchost.exe -k netsvcs
0036      0 SYSTEM_START RUNNING    DRV  KERNEL  000000b74db0 blbdrive                                        ---            system32\DRIVERS\blbdrive.sys      :: \Driver\blbdrive
0037      0 DEMAND_START RUNNING    DRV  FS      000000b74ea0 bowser / Browser Support Driver                 ---            system32\DRIVERS\bowser.sys        :: \FileSystem\bowser
...
```

### File: by-id/[id]/svcinfo.txt &nbsp;&nbsp;and&nbsp;&nbsp; by-name/[name]/svcinfo.txt

The file svcinfo.txt contains detailed information about each service as shown below:

```
Ordinal:          35
Service Name:     BITS
Display Name:     Background Intelligent Transfer Service
Record Address:   0x000000b74cc0
Service Type:     SERVICE_AUTO_START (0x2)
Service State:    SERVICE_RUNNING (0x4)
Service Type:     SERVICE_WIN32_SHARE_PROCESS (0x20)
Process ID (PID): 112
Path:             C:\Windows\system32\svchost.exe -k netsvcs
Image Path:       %SystemRoot%\System32\qmgr.dll
User Type:        
User Account:     SYSTEM
```

### Example

The example shows the _sys/services_ directory with the summary information and detailed information about one service.

[[resources/root_sysinfo_services.png]]

### For Developers
The _sys/svc_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_svc.c_ in the vmm project.

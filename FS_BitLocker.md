## The `misc/bitlocker` directory

The directory _misc/bitlocker_ exists as a sub-directory to the file system root.

The directory contains identified bitlocker encryption keys in formats which allows for easy unlocking of the bitlocker volumes.

The files in the **misc/bitlocker** directory are listed in the table below:
| File                           | Description                                               |
| ------------------------------ | --------------------------------------------------------- |
| readme.txt                     | General information about the bitlocker module.           |
| <address>.bin                  | The binary in-memory representation of a key.             |
| <address>.fvek                 | Dislocker unlock key (see below).                         |
| <address>.txt                  | Text representation of a key.                             |

Files in the _misc/bitlocker_ directory is read-only.

### Information

The bitlocker plugin is loosely based on the excellent [bitlocker volatility](https://github.com/breppo/Volatility-BitLocker) plugin. The MemProcFS plugin uses the same underlying technique of identifying potential bitlocker keys by pool tagging and other heuristics. The MemProcFS plugin also does some post-processing to increase output quality.

The bitlocker plugin works quite well on Windows 7 and Windows 10/11. Issues however exists on Windows 8 (and early Windows 10) versions where multiple keys may be recovered in error. At least one key should however most often be correct even on Windows 8 and early Windows 10 versions.

In order to mount a recovered bitlocker key it's recommended to use dislocker on a Linux system. Please use the recovered _.fvek_ key.
```
dislocker -k <recovered_key>.fvek /path/to/disk /path/to/dislocker          
mount /path/to/dislocker/dislocker-file /path/to/mount
```
Please see an example of the mount process using dislocker in the example section below.

### Example

The example shows the _misc/bitlocker_ directory with a recovered bitlocker key.

[[resources/root_misc_bitlocker_1.png]]

The image below shows how its possible to mount a bitlocker encrypted drive by using dislocker and the recovered _.fvek_ key.

[[resources/root_misc_bitlocker_2.png]]

### For Developers
The _misc/bitlocker_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_misc_bitlocker.c_ in the vmm project.

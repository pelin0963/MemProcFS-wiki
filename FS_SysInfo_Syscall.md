## The `sys/syscall` directory

The directory _sys/syscall_ exists as a sub-directory to the file system root.

The directory contain files with the syscalls of the system. The files are derived from parsing the System Service Dispatch Table (SSDT).

The files in the **sys/syscall** directory are listed in the table below:
| File                  | Description                                                             |
| --------------------- | ----------------------------------------------------------------------- |
| syscall_nt.txt        | Syscall table for kernel (ntoskrnl.exe) functionality.                  |
| syscall_nt_shadow.txt | Syscall shadow table for kernel (ntoskrnl.exe) functionality.           |
| syscall_win32k.txt    | Syscall table for GDI/GUI (win32k.sys) functionality.                   |

Files in the _sys/syscall_ directory are read-only.


### Files: syscall_nt.txt, syscall_nt_shadow.txt and syscall_win32k.txt:
The files syscall_nt.txt, syscall_nt_shadow.txt and syscall_win32k.txt contains syscall information as described below:

```
SYSCALL#  TABLE_DATA  OFFSET   VIRTUAL_ADDRESS   TYPE   FUNCTION_NAME
=====================================================================
0000      fced6104    +1122d0  fffff8070f1122d0  nt     NtAccessCheck
0001      fcf76a00    +11c360  fffff8070f11c360  nt     NtWorkerFactoryWorkerReady
0002      02b81c02    +6dce80  fffff8070f6dce80  nt     NtAcceptConnectPort
...
14e7      ff9c43a5    +00843a  ffff80956fd9843a  win32k NtVisualCaptureBits
14e8      ff99d380    +005d38  ffff80956fd95d38  win32k NtUserSetClassLongPtr
14e9      ff99d4a0    +005d4a  ffff80956fd95d4a  win32k NtUserSetWindowLongPtr
```
Where TABLE_DATA is the raw entry in the SSDT and OFFSET is the function offset from the start of either ntoskrnl.exe or win32k.sys.

### Example

The example shows the _sys/syscall_ directory with the files _syscall_nt.txt_ and _syscall_win32k.txt_. The files contains the syscall tables for the kernel syscalls (nt) and the GUI related syscalls (win32k).

[[resources/root_sysinfo_syscall.png]]


### For Developers
The _sys/syscall_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_syscall.c_ in the _vmm_ project.


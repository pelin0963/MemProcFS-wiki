## Developing native C plugins

Developing native C plugins allows for the addition of highly efficient custom parsing functionality to be dropped in the plugin directory. The Memory Process File System will try to load the module. If the module so chooses it may register one or more callback directories both in the root directory and per-process directories.

The plugin `.dll`/`.so` should be placed in the plugins directory and be named on the form `m_*.dll`/`m_*.so` in order for them to be loaded. They must also export the function _InitializeVmmPlugin_ function as per below:

```C
__declspec(dllexport)
VOID InitializeVmmPlugin(_In_ VMM_HANDLE H, _In_ PVMMDLL_PLUGIN_REGINFO pRegInfo)
{
    if((pRegInfo->magic != VMMDLL_PLUGIN_REGINFO_MAGIC) || (pRegInfo->wVersion != VMMDLL_PLUGIN_REGINFO_VERSION)) { return; }
    // Ensure that the plugin support the memory model that is used. The plugin
    // currently supports the 64-bit x64 and 32-bit x86 and x86-pae memory models.
    if(!((pRegInfo->tpMemoryModel == VMMDLL_MEMORYMODEL_X64) || (pRegInfo->tpMemoryModel == VMMDLL_MEMORYMODEL_X86) || (pRegInfo->tpMemoryModel == VMMDLL_MEMORYMODEL_X86PAE))) { return; }
    // It is also possible to detect on the system - but no need in the 'vmemd' plugin.
    // if((pRegInfo->tpSystem != VMM_SYSTEM_WINDOWS_X64) && (pRegInfo->tpSystem != VMM_SYSTEM_WINDOWS_X86)) { return; }
    strcpy_s(pRegInfo->reg_info.szModuleName, 32, "vmemd");     // module name - 'vmemd'.
    pRegInfo->reg_info.fProcessModule = TRUE;                   // module shows in process directory.
    pRegInfo->reg_fn.pfnList = VMemD_List;                      // List function supported.
    pRegInfo->reg_fn.pfnRead = VMemD_Read;                      // Read function supported.
    pRegInfo->reg_fn.pfnWrite = VMemD_Write;                    // Write function supported.
    pRegInfo->pfnPluginManager_Register(pRegInfo);              // Register with the plugin manager.
}
```

The above example is taken from the example plugin _m_vmemd.dll_/_m_vmemd.so_ which registers itself in every per-process directory as _vmemd_. All struct definitions and API callback functionality may be found in the `vmmdll.h` header file. The plugin must implement a `List` function and a `Read` function. The `Write`, `Notify` and `Close` functions are optional.

It is recommended that each native plugin, in its Initialize function, first checks whether it supports the target system being analyzed before registering with the Plugin Manager.

## Developing Rust plugins

Developing MemProcFS plugins in Rust will give you the performance of native code while avoiding common programming pitfalls such as race conditions and memory issues.

A Rust plugin is a Rust library with the crate type `cdylib`. The library should export/expose the native C-function `InitializeVmmPlugin` which MemProcFS will call to register the plugin. The resulting plugin .dll/.so file should be named m_<pluginname>.dll or m_<pluginname>.so and be placed in the MemProcFS plugins sub-directory.

For more information check out the [example plugin](https://github.com/ufrisk/MemProcFS/tree/master/vmmrust/m_example_plugin) and especially its [Cargo.toml](https://github.com/ufrisk/MemProcFS/blob/master/vmmrust/m_example_plugin/Cargo.toml) file and the plugin example library [source code](https://github.com/ufrisk/MemProcFS/blob/master/vmmrust/m_example_plugin/src/lib.rs).

Also check out the [MemProcFS API documentation](https://docs.rs/memprocfs/) at docs.rs.


## Examples:

A small example of the `InitializeVmmPlugin` plugin entry point is found below. Check out the [example project]() for a more complete documentation.
```Rust
use memprocfs::*;

#[no_mangle]
pub extern "C" fn InitializeVmmPlugin(native_h : usize, native_reginfo : usize) {
    let (system_info, mut plugin_init_ctx) =
        match new_plugin_initialization::<PluginContext>(native_h, native_reginfo) {
            Ok(r) => r,
            Err(_) => return,
        };

    let ctx = PluginContext {
        // ...
    };
    plugin_init_ctx.ctx = Some(ctx);
    plugin_init_ctx.is_root_module = true;
    plugin_init_ctx.is_process_module = true;
    plugin_init_ctx.path_name = String::from("/rust/example");

    plugin_init_ctx.fn_list = Some(plugin_list_cb);
    plugin_init_ctx.fn_read = Some(plugin_read_cb);
    plugin_init_ctx.fn_write = Some(plugin_write_cb);

    let _r = plugin_init_ctx.register();
}
```
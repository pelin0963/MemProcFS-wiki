## The `root` directory

The root directory of the Memory Process File System contains multiple directories and files which contains the physical memory of the target.

#### Directories:
The files in the **root** directory are listed in the table below:
| Directory                                                        | Description                                        |
| ---------------------------------------------------------------- | -------------------------------------------------- |
| [conf](https://github.com/ufrisk/MemProcFS/wiki/FS_Conf)         | Configuration and Status.                          |
| [forensic](https://github.com/ufrisk/MemProcFS/wiki/FS_Forensic) | Forensic mode.                                     |
| [misc](https://github.com/ufrisk/MemProcFS/wiki/FS_Misc)         | Miscellaneous functionality                        |
| name                                                             | Per-process directories listed by process name.    |
| pid                                                              | Per-process directories listed by process pid.     |
| py                                                               | Python based plugins.                              |
| [registry](https://github.com/ufrisk/MemProcFS/wiki/FS_Registry) | Registry information.                              |
| [sys](https://github.com/ufrisk/MemProcFS/wiki/FS_SysInfo)       | System information.                                |
| [vm](https://github.com/ufrisk/MemProcFS/wiki/VM)                | Virtual Machine (VM) information.                  |

#### Files:

The files in the **root** directory are listed in the table below:
| File              | Description                                                             |
| ----------------- | ----------------------------------------------------------------------- |
| memory.dmp        | The physical memory slightly adjusted to conform with the Microsoft crash dump format and WinDbg. |
| memory.pmem       | The raw physical memory.                                                |

The files are writable if a write-capable memory acquisition device is used.


### Example

The example below shows hex editing of the _memory.pmem_ file which reflects the physical memory of the target being analyzed. In this example the low stub is being analyzed and the kernel page table base (PML4) is marked at address 0x10a0.

Also shown is WinDbg accessing the auto-generated _memory.dmp_ WinDbg compatible full crash dump file.

[[resources/root_root_2.png]]

### For Developers
It is possible to add sub-directories if registering general/root functionality in native plugins.
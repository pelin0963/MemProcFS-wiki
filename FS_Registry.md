## The `registry` directory

The directory _registry_ exists as a sub-directory to the file system root.

#### Registry hive files

Registry hive files are listed in the **registry\\hive_files\\** sub-directory. Hive files are reconstructed from memory fragments with a best-effort algorithm. Hive files may be partially corrupt due to unavailable paged-out memory pages being zero-padded. It's possible to open hive files with 3rd party tools like Registry Explorer.

The registry hive files are writable if a write-capable memory acquisition device is used. Writes will be written into memory of the target system - not onto the disk. Writing may cause registry corruption.

#### Registry by-hive

All enumerated registry hives are listed in the **registry\\by-hive\\** sub-directory. Each hive contains a _ROOT key_ for the normal registry and an _ORPHAN key_ which may contain registry keys without known parent keys (due to deletion or missing registry memory).

Registry key are represented as directories. The last write timestamp of the registry key is reflected in the last modified timestamp of the directory. Deleted keys may show up as compressed directories. If multiple keys with the same name are enumerated they are represented with a suffix of 1 to 9.

Each registry value is represented as two files - one binary representation of the binary data without a file ending and one text file containing a human readable representation including the registry key type.

Registry keys and values in the file system are read only. To edit the registry in-memory please edit the hive files.

#### Registry HKLM

The registry folder **registry\\HKLM\\** provides a stable path/shortcut to common registry hives grouped under _HKEY_LOCAL_MACHINE_. This view is otherwise the same as the **registry\\by-hive\\** view.



### Examples

The example below shows a user exploring the hardware hive in the **registry\HKLM\HARDWARE\\** folder. Text files contains a human readable summary of the registry value type and its content. Non text files show the binary value data. The data modified time stamp shows the registry key last write time.

[[resources/registry_key.png]]

The example below shows the **registry\\hive_files\\** directory and its contents in the form of enumerated registry hive files. The registry hive **SAM** is also opened in the 3rd party tool Registry Explorer.

[[resources/registry_hive_sam.png]]



### For Developers
The _registry_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_winreg.c_ in the _vmm_ project.

Also check the [C/C++ API](./API_C) and the [Python API](./API_Python) wiki sections for information about the registry API.

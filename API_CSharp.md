## C# API

Most functionality in the Memory Process File System is made available in a C# API for the use by developers. The C# API is a wrapper for the C/C++ API. Since the MemProcFS native library is 64-bit only the C# process must be running in 64-bit mode to be able to make use of the C# library.

The C# API is located in `vmm_example.cs` in the `vmmsharp` project.

The basic read/write physical memory exported by the [LeechCore library](https://github.com/ufrisk/) is also implemented in the `vmmsharp` project.

The complete documentation is found in `cmmsharp.cs`. This wiki entry contains an overview of the C# API.

### Example:

An example file containing a lot of use cases are found in the file `vmm_example.cs` in the vmmsharp project in the visual studio solution.

### Functionality:

The functionality mirror the functionality of the C/C++ API at large. Please consult the C# API sources or the C/C++ API documentation for information and guidance.

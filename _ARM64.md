The MemProcFS on ARM64 Windows
==============================

#### This entry applies to running MemProcFS under Windows on ARM64.
This includes Windows on Apple Silicon - i.e. Windows on Parallels or VMWare Fusion, but also other ARM devices running Windows.

#### For instructions regarding Linux please check out [MemProcFS on Linux](https://github.com/ufrisk/MemProcFS/wiki/_Linux)

#### Limitations:
* DMA is currently not supported due to lack of driver support from [FTDI](https://ftdichip.com/drivers/d3xx-drivers/).
* Memory analysis of arm64 memory dumps are not yet supported.

#### Features:
* Full support for analyzing x86, x64 and arm64 memory dump files on arm64 Windows.

## Installation Instructions
The x64 version of MemProcFS works on arm64 devices with decent performance. It's also possible to build MemProcFS for arm64 natively by yourself.
1) Install the [Dokany virtual file system](https://github.com/dokan-dev/dokany/releases/latest) driver (DokanSetup.exe).
2) Unzip [MemProcFS x64 Windows release](https://github.com/ufrisk/MemProcFS/releases/latest) to a folder of your choosing.
3) Run MemProcFS! **Example:** `memprocfs.exe -device c:\dumps\yourmemorydumpfile.raw`

If the above fails with Dokany error -3 this means that the dokany driver have failed to properly install. In that case see points 4-8 below:

4) Download dokan.zip from [Dokany releases](https://github.com/dokan-dev/dokany/releases/latest).
5) Unzip dokan.zip
6) Install driver:
   6.1. move into dokan\ARM64\Release\Driver\sys
   6.2. right click dokan.inf and choose in the popup menu: Install
7) Start driver from administrative elevated command prompt run: `sc.exe create dokan2.sys binPath=C:\windows\system32\drivers\dokan2.sys type=kernel && sc.exe start dokan2.sys` This is only required once.
8) Run MemProcFS!

**Please note that it is not possible to perform DMA attacks using MemProcFS on arm64 Windows due to the current lack of driver support.**

It is possible to analyze arm64 memory dumps, such as crashdump (.dmp) files, VMWare (.vmem/.vmsn) and raw memory dump files. If analyzing a raw memory dump file specify the additional start-up option: `-arch arm64`.

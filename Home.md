The MemProcFS Wiki and Guide
=============================================

Welcome to the MemProcFS Wiki and Guide! In addition to this guide please check out the project [README](../blob/master/README.md) for general information and installation instructions.

For individual guide items please have a look at the sidebar to the right containing information on a wide area of functionality. It is recommended to start looking at the command line and file system **guide found in the menu to the right.**

If you are interested in using MemProcFS as a part of your own project - or just script against it please have a look at the API functionality for [C/C++](wiki/API_C), [C#](wiki/API_CSharp), [Java](wiki/API_Java), [Rust](wiki/API_Rust) and [Python](wiki/API_Python).

If you are interesting in creating your own plugin functionality please have a look in the plugins sections in this wiki. Well documented example plugins exist for C, Rust and Python.

<p align="center"><img src="https://github.com/ufrisk/MemProcFS/wiki/resources/proc_base3.png" height="190"/><img src="https://github.com/ufrisk/MemProcFS/wiki/resources/proc_modules.png" height="190"/></p>
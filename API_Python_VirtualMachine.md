## Overview:

The MemProcFS provides access to child virtual machines parsed from the host memory.

For general information about Virtual Machine parsing and the current limitations please consult the [Virtual Machine information](https://github.com/ufrisk/MemProcFS/wiki/VM).

For Virtual Machines to be parsed `memprocfs.Vmm()` must have been initialized with one of the arguments: `-vm` / `-vm-basic` / `-vm-nested`. The VM API is used to provide easy access to virtual machines. Generally it's possible to retrieve the VM name and read VM memory.

If the VM is detected as a Windows VM it's also possible to analyze the sub-vm.


## VmmVirtualMachine:

Represents a virtual machine.

### Sources:
* [`vmm.maps.virtualmachines()`](API_Python_Base#Methods) - list of virtual machine objects.

### Attributes:
```Python
virtualmachine.is_active    # bool: is the virtual machine active?
virtualmachine.is_physical  # bool: is the virtual machine a physical only virtual machine?
virtualmachine.is_readonly  # bool: is the virtual machine memory read-only or read-write?
virtualmachine.max_memory   # int: maximum guest physical memory address.
virtualmachine.name         # string: the name of the virtual machine.
virtualmachine.os_build     # int: build number of the guest windows virtual machine (0 if not available).
virtualmachine.type         # int: the virtual machine type as given by VMMDLL_VM_TP_*.
```

### Methods:
```Python

# Create a new Vmm sub-analysis handle to analyze this Windows virtual machine.
# It's best to only call this function once per virtual machine and re-use the
# created Python Vmm object since this is a very performance intense operation.
# Please note that this is only possible on active Windows virtual machines.
-- return
virtualmachine.Vmm() # -> Vmm
# example:
#   virtualmachine.Vmm() # -> Vmm


# Read virtual machine guest physical memory.
# -- address_guest_physical
# -- size_to_read
# -- return
virtualmachine.read(int: address_guest_physical, int: size_to_read) # -> bytes
# example:
#   print(vmm.hex( virtualmachine.read(0x45000, 0x20) )) ->
#   0000    4d 5a 90 00 03 00 00 00  04 00 00 00 ff ff 00 00   MZ..............
#   0010    b8 00 00 00 00 00 00 00  40 00 00 00 00 00 00 00   ........@.......


# Write data (if supported) to the virtual machine guest physical memory.
# -- address_guest_physical
# -- data
vmm.memory.write(int: address_guest_physical, bytes: data) # -> None
# example:
#   virtualmachine.write(0x1000, b'0000')

```

Please see **[`VmmScatterMemory`](API_Python_ScatterMemory)** for information about how to use the `VmmScatterObject` returned by function `vmm.memory.scatter_initialize(opt int: flags)` which is documented below.

```Python
# Initialize a Scatter Virtual Memory Read object which is used to simplify efficient memory reads.
# This is accomplished by using the simplified native MemProcFS VMMDLL_Scatter_* functionality.
# -- flags = flags as specified by memprocfs.FLAG_*.
# -- return
virtualmachine.scatter_initialize(opt int: flags) # -> VmmScatterMemory
# example:
#   vmm.memory.scatter_initialize(memprocfs.FLAG_NOCACHE) -> VmmScatterMemory

```

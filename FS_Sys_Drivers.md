## The `sys/drivers` directory

The directory _sys/drivers_ exists as a sub-directory to the file system root.

The directory and its sub-directories contains information about loaded active kernel drivers and their devices.

The files in the **sys/drivers** directory are listed in the table below:
| File                           | Description                                               |
| ------------------------------ | --------------------------------------------------------- |
| devices.txt                    | Summary information about all devices.                    |
| drivers.txt                    | Summary information about all drivers.                    |
| driver_irp.txt                 | Information about IRPs related to all drivers.            |
| by-name/[driver]/*             | Information about the Driver object and its header.       |
| by-name/[driver]/[module]*     | Information about the Driver module.                      |

Files in the _sys/drivers_ directory and sub-directories are read-only.



### File: devices.txt

The file _devices.txt_ contains summary information about loaded active kernel devices in a device tree format. The meaning of the different columns are as follows:

```           
   # Depth DeviceAddress     DeviceName          DriverAddress DriverName       DeviceType / ExtraInfo
------------------------------------------------------------------------------------------------------
0000 - ffffbb8c5ba6e280      00000015         ffffbb8c5bb91970 ACPI             FILE_DEVICE_ACPI(50)
0001 - ffffbb8c5ba8f3d0      RESOURCE_HUB     ffffbb8c5bb56780 acpiex           FILE_DEVICE_UNKNOWN(34)
0002 - ffffbb8c5baad0a0      KMDF0            ffffbb8c5bbde9e0 Wdf01000         FILE_DEVICE_UNKNOWN(34)
0003 - ffffbb8c5bab3ca0      KsecDD           ffffbb8c5bc43060 KSecDD           FILE_DEVICE_KSEC(57)
...
009d - ffffbb8c5cbf1030      ---              ffffbb8c5c94ce00 volsnap          FILE_DEVICE_DISK(7)
009e -- ffffbb8c5c969de0     ---              ffffbb8c5c94be00 volume           FILE_DEVICE_DISK(7)
009f --- ffffbb8c5ca618d0    ---              ffffbb8c5c94de00 rdyboost         FILE_DEVICE_DISK(7)
00a0 ---- ffffbb8c5cbf0930   ---              ffffbb8c5c95ae00 iorate           FILE_DEVICE_DISK(7)
00a1 ----- ffffbb8c5cbef030  ---              ffffbb8c5c940e00 fvevol           FILE_DEVICE_DISK(7)
00a2 ------ ffffbb8c5caa18f0 HarddiskVolume3  ffffbb8c5bb804f0 volmgr           FILE_DEVICE_DISK(7) / DeviceFS:[ffffbb8c5cc1d030] VolumeLabel:[SYSTEMDRIVE]
...
```



### File: drivers.txt

The file _drivers.txt_ contains summary information about loaded active kernel drivers. The meaning of the different columns are as follows:

```
   #   Object Address Driver          Size Drv Range: Start-End              Service Key      Driver Name
---------------------------------------------------------------------------------------------------------
0000 ffffcf00cb5f5450 ahcache        41000 fffff809b6240000-fffff809b6280fff ahcache          \Driver\ahcache
0001 ffffcf00cb337e60 iorate         11000 fffff809b6480000-fffff809b6490fff iorate           \Driver\iorate
0002 ffffcf00c9ff5ac0 pcw            13000 fffff809b59e0000-fffff809b59f2fff pcw              \Driver\pcw
0003 ffffcf00c9f61c40 VBoxNetLwf     49000 fffff809b6b20000-fffff809b6b68fff VBoxNetLwf       \Driver\VBoxNetLwf
0004 ffffcf00c9819390 lxss            b000 fffff809b5420000-fffff809b542afff lxss             \Driver\lxss
0005 ffffcf00cdd3b060 rspndr         1a000 fffff809bb420000-fffff809bb439fff rspndr           \Driver\rspndr
0006 ffffcf00cd6d2e60 WINUSB          d000 fffff809bb180000-fffff809bb19cfff WINUSB           \Driver\WINUSB
0007 ffffcf00cb312d20 volume          b000 fffff809b7380000-fffff809b738afff volume           \Driver\volume
0008 ffffcf00c9547e60 SoftwareDevice     0 fffff8032ae89000-fffff8032ae89000 \Driver\Software \Driver\SoftwareDevice
...
```



### File: driver_irp.txt

The file _driver_irp.txt_ contains information about each of the IRPs tied to a driver.

Please note that unsupported requests point to the default unsupported function in the kernel. This is denoted as `---` in the example below. Unknown target modules are listed as `?`.

The meaning of the different columns are as follows:

```
   # Driver            # IRP_MJ_*                          Address Target Module
--------------------------------------------------------------------------------
0000 ahcache           0 CREATE                   fffff809b626a610 ahcache
0001 ahcache           1 CREATE_NAMED_PIPE        fffff8032aece2c0 ---
0002 ahcache           2 CLOSE                    fffff809b626a610 ahcache
0003 ahcache           3 READ                     fffff8032aece2c0 ---
0004 ahcache           4 WRITE                    fffff8032aece2c0 ---
0005 ahcache           5 QUERY_INFORMATION        fffff8032aece2c0 ---
0006 ahcache           6 SET_INFORMATION          fffff8032aece2c0 ---
0007 ahcache           7 QUERY_EA                 fffff8032aece2c0 ---
0008 ahcache           8 SET_EA                   fffff8032aece2c0 ---
0009 ahcache           9 FLUSH_BUFFERS            fffff8032aece2c0 ---
000a ahcache          10 QUERY_VOLUME_INFORMATION fffff8032aece2c0 ---
000b ahcache          11 SET_VOLUME_INFORMATION   fffff8032aece2c0 ---
000c ahcache          12 DIRECTORY_CONTROL        fffff8032aece2c0 ---
000d ahcache          13 FILE_SYSTEM_CONTROL      fffff8032aece2c0 ---
000e ahcache          14 DEVICE_CONTROL           fffff809b6260870 ahcache
000f ahcache          15 INTERNAL_DEVICE_CONTROL  fffff8032aece2c0 ---
0010 ahcache          16 SHUTDOWN                 fffff809b6278190 ahcache
0011 ahcache          17 LOCK_CONTROL             fffff8032aece2c0 ---
0012 ahcache          18 CLEANUP                  fffff8032aece2c0 ---
0013 ahcache          19 CREATE_MAILSLOT          fffff8032aece2c0 ---
0014 ahcache          20 QUERY_SECURITY           fffff8032aece2c0 ---
0015 ahcache          21 SET_SECURITY             fffff8032aece2c0 ---
0016 ahcache          22 POWER                    fffff8032aece2c0 ---
0017 ahcache          23 SYSTEM_CONTROL           fffff8032aece2c0 ---
0018 ahcache          24 DEVICE_CHANGE            fffff8032aece2c0 ---
0019 ahcache          25 QUERY_QUOTA              fffff8032aece2c0 ---
001a ahcache          26 SET_QUOTA                fffff8032aece2c0 ---
001b ahcache          27 PNP                      fffff8032aece2c0 ---
001c iorate            0 CREATE                   fffff809b6484860 iorate
001d iorate            1 CREATE_NAMED_PIPE        fffff809b6484860 iorate
001e iorate            2 CLOSE                    fffff809b6484860 iorate
...
```



### Example

The example shows the loaded kernel driver list and its IRPs - which are both made available under the _sys/drivers_ directory.

[[resources/root_sys_drivers.png]]



### For Developers
The _sys/drivers_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_driver.c_ in the vmm project.

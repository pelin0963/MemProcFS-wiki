## The `sys/pool` directory

The directory _sys/pool_ exists as a sub-directory to the file system root.

The directory and its sub-directories contains information about allocations in the kernel NonPagedPool and PagedPool.

The files in the **sys/objects** directory are listed in the table below:
| File                           | Description                                               |
| ------------------------------ | --------------------------------------------------------- |
| readme.txt                     | Readme with information about the pool parsing.           |
| big/allocations.txt            | Pool allocations in the big pool table >= 1kB             |
| all/allocations.txt            | All pool allocations (incl. big pool allocations)         |

Each pool allocation is sorted by its pool tag and address in a by-tag sub-directory.
| File                           | Description                                               |
| ------------------------------ | --------------------------------------------------------- |
| allocations.txt                | Pool allocations related to the specific pool tag.        |
| [addr]/pool-address.txt        | The pool allocation address.                              |
| [addr]/pool-data.txt           | The pool allocated data (binary file).                    |
| [addr]/pool-tag.txt            | The pool tag.                                             |

NB! pool allocations may be missing and invalid pool allocations may also exist. Pool allocations may also be assigned the wrong pool and type. This is primarily an issue on systems prior to Windows 10 1903 release.

Files in the _sys/pool_ directory and sub-directories are read-only.



### File: allocations.txt

The file allocations.txt_ contains summary information about the pool allocations. The meaning of the different columns are as follows:

```
       #  Tag A         Address      Size Type Pool      
---------------------------------------------------      
       0 AfdB A ffffd582c5d02000    67000 Big  NonPaged  
       1 AfdB A ffffd582c5d69000    68000 Big  NonPaged  
       2 MiIo A ffffd582c6253040      150 Vs   NonPagedNx
       3 Mm   A ffffd582c62531a0      250 Vs   NonPagedNx
       4 VfIT A ffffd582c6253400      1b0 Vs   NonPagedNx
       5 VfAT A ffffd582c62535c0       70 Vs   NonPagedNx
...
```



### Example

The example shows the general 'all' pool allocations as well as a select pool tag, its allocations and a specific pool allocation.

[[resources/root_sys_pool.png]]



### For Developers
The _sys/pool_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_pool.c_ in the vmm project.



### Special Thanks:
Special thanks to Yarden Shafir [@yarden_shafir](https://twitter.com/yarden_shafir) for the excellent [Black Hat talk](https://i.blackhat.com/USA21/Wednesday-Handouts/us-21-Windows-Heap-Backed-Pool-The-Good-The-Bad-And-The-Encoded.pdf) from which most of the Windows 10 1903+ implementation is based upon. Please also check out Yarden Shafirs excellent [Pool Viewer tool](https://github.com/yardenshafir/PoolViewer) on Github!


## The `.status` per-process directory

The directory _.status_ exists as a sub-directory in each process directory.

The file contains Memory Process File System status and configuration settings related to each process. The settings are exposed as files - please find them listed below:

* **cache_file_enable:** in-memory caching of memory pages when reading/writing memory in this process - **always user writable!**

NB! the _cache_file_enable_ file will allow a user to disable read caching of memory on a per-process basis. This is only a meaningful setting if memory may change - such as when analyzing live memory with a write-capable memory acquisition device. This is not a meaningful setting (even though it works) if never changing read-only memory devices, such as memory dump files, are analyzed.

### Example

The example below shows reading and writing to the _cache_file_enable_ in the _.status_ process directory.

[[resources/proc_status.png]]

### For Developers
The _.status_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _m_status.c_ in the _vmm_ project. In addition of being responsible for the per-process status and configuration settings it's responsible for global status and configuration.
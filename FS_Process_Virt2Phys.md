## The `virt2phys` per-process directory

The directory _virt2phys_ exists as a sub-directory in each process directory.

The virt2phys directory contains one special file named _virt.txt_ that the user of MemProcFS may write a virtual address into. Once saved the other files will automatically update to reflect the user-selected virtual address written to the _virt.txt_ file.

NB! Memory may still be readable even though virt2phys may not find it if the memory is "paged out". The virt2phys looks at currently active memory only.

| File            | Description                                                             |
| --------------- | ----------------------------------------------------------------------- |
| virt.txt        | Virtual address in hex - **always user writable!**                      |
| phys.txt        | Physical address (in hex) that the virtual address maps to              |
| map.txt         | virtual to physical translation map - showing page table entries and their locations in the PML4, PDPT, PD and PT page tables. |
| readme.txt      | Informational README file.                                              |
| pt_pml4.mem*    | PML4 page table                                                         |
| pt_pdpt.mem*    | PDPT page table                                                         |
| pt_pd.mem*      | Page Directory page table                                               |
| pt_pt.mem*      | Page Table page table                                                   |
| page.mem        | 4kB page that the virt address maps to (or corresponding 4kB section of memory if large pages are used)                        |

*) On arm64 architectures the page table memory files are named pt_lvl0.mem, pt_lvl1.mem, pt_lvl2.mem and pt_lvl3.mem.

The _virt.txt_ file is always writable. The _map.txt_ file is always read-only while all other files are writable if a write-capable memory acquisition device is used.

### Example

The example below shows the files in the _virt2phys_ sub-directory of the _explorer.exe_ process. The virtual memory address _00007ff75fc50000_ is echoed into the _virt.txt_ file. The page table walk is shown by viewing the map file with the `cat map.txt` command. The resulting physical address of _0x1a6c96000_ is shown by viewing the _phys.txt_ file with the `cat phys.txt` command. Also shown, in the HxD hex editor, is the 4th level page table for the virtual address.

The first column of the map file shows which page table. Then the physical address of the page table is shown next. Then the offset (in bytes) of the page table entry and at last the page table entry itself (PML4E/PDPTE/PDE/PTE) is shown.

[[resources/proc_virt2phys.png]]

### For Developers
The _virt2phys_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_proc_virt2phys.c_ in the _vmm_ project. The plugin contains limited caching functionality that will allow it to store the contents of virt file even though the process list may be refreshed in a read/write scenario.
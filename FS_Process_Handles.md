## The `handles` per-process directory

The directory _handles_ exists as a sub-directory in each process directory.

The _handles_ directory have one file - **handles.txt** which contains information about open process handles, such as files, mutexes, registry keys and more. The current, relatively limited, handle functionality will be expanded upon in the future.

The _handles_ directory have one directory - **by-id** which contains additional handle information by handle id.

### File: handles.txt

The file sections contains information about open process handles. The meaning of the different columns are as follows:

```
#       PID  Handle Object Address   Access Type             Description
========================================================================
000c   5512      34 ffff83073ed52c00      3 Directory        KnownDlls
...
0028   5512      a4 ffff978117262ca0  f037f WindowStation    WinSta0
0029   5512      a8 ffff97811ab21ed0 120089 File             \Windows\System32\en-GB\notepad.exe.mui
002a   5512      ac ffff97811ab25270    804 EtwRegistration
002b   5512      b0 ffff830744073cb0  20019 Key
....
00ec   5512     3c0 ffff97811aab3b20  f0003 IoCompletionRese
00ed   5512     3c4 ffff97811ab83080 1fffff Thread           TID 5516
00ee   5512     3c8 ffff97811a94ec70 1f0001 ALPC Port        [CoreUI]-PID(5512)-TID(5516)
00ef   5512     3cc ffff9781198bccd0      1 WaitCompletionPa
```

### Example

The example below shows open handles in the _handles/handles.txt_ for the _cmd.exe_ process. Some files, directories, and events are shown.

[[resources/proc_handles.png]]

### For Developers
The _handles_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_proc_handles.c_ in the _vmm_ project.

The handle map, which is exported in vmm.dll, contains additional information such as number of pointers to the individual handles which is now shown in _handles.txt_.

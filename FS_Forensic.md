## The `forensic` directory

The directory _forensic_ exists as a sub-directory to the file system root.

The directory contains directories and files related to the MemProcFS forensic sub-system.

The forensic sub-system is a collection of more thorough batch-oriented analysis tasks that may be undertaken on memory dumps. By default the forensic mode is not enabled.

Forensic mode may be enabled either via the command line parameter `-forensic` or by editing the file `forensic_enable.txt`. Both the command line parameter and the file takes a number between 1-4 to start forensic mode.

The forensic mode may take some time to analyze. It will read the complete memory dump sequentially in one pass and perform multiple analysis tasks in parallel. The result will be saved to a SQLITE database. Depending on which number 1-4 was selected for startup one may open the sqlite database in the sqlite browser upon finish. Upon finish Timelines and NTFS MFT analysis amongst other things will be made available as sub-directories to the forensic directory.

**Note! The forensic sub-system is not recommended for live memory - such as memory retrieved by the PCILeech FPGA devices due to drift/smear. Forensic mode for live memory is supported with degraded quality of results only.**

<br>The files in the **forensic** directory are listed in the table below:
| File                 | Access     | Description                                           |
| -------------------- | ---------- | ----------------------------------------------------- |
| database.txt         | Read       | Path and name of sqlite database (if exists).         |
| forensic_enable.txt  | Read/Write | Forensic mode number (see table below).               |
| progress_percent.txt | Read       | Progress indicator in percent % of forensic progress. |
| readme.txt           | Read       | Readme file.                                          |


<br>The values of the **forensic_enable.txt** file and **-forensic** startup parameter are listed in the table below:
| Value | Description                                                              |
| ----- | ------------------------------------------------------------------------ |
| 0     | Forensic mode not started or failed to start.                            |
| 1     | Forensic mode with in-memory only SQLITE database.                       |
| 2     | Forensic mode with temporary SQLITE database deleted upon MemProcFS exit.|
| 3     | Forensic mode with temporary SQLITE database kept upon MemProcFS exit.   |
| 4     | Forensic mode with well known SQLITE database kept upon MemProcFS exit.  |

### Example

The example shows the **forensic/** directory, with the file _forensic_enable.txt_ in notepad.

[[resources/root_forensic.png]]

### For Developers
The _forensic_ directory is implemented as a built-in native C-code plugin. The plugin source is located in the vmm project in the file _modules/m_fc.c_.

## The `sys/net` directory

The directory _sys/net_ exists as a sub-directory to the file system root.

The directory contains networking related information.

The files in the **sys/net** directory are listed in the table below:
| File              | Description                                                                            |
| ----------------- | -------------------------------------------------------------------------------------- |
| net/netstat.txt   | Netstat list of TCP connections.                                                       |
| net/netstat-v.txt | Netstat verbose version - also list time, kernel object address and process image path.|

Files in the _sys/net_ directory are read-only.

### File: net/netstat.txt
The file net/netstat.txt contains a listing of active TCP connections similar to _netstat -ano_.

```
TCPv4  10.8.0.101:53176  10.8.0.5:445          ESTABLISHED      4  System
TCPv4  127.0.0.1:58326   127.0.0.1:58325       ESTABLISHED   2936  firefox.exe
TCPv4  10.8.0.101:57372  40.67.251.132:443     ESTABLISHED   3796  svchost.exe
TCPv4  10.8.0.101:58523  169.254.164.112:7680  SYN_SENT      4192  svchost.exe
TCPv6  [::1]:58228       [::1]:28473           ESTABLISHED  10416  MemProcFS.exe
TCPv6  [::1]:58231       [::1]:28473           ESTABLISHED  10416  MemProcFS.exe
TCPv4  10.8.0.101:57644  13.93.117.220:443     ESTABLISHED  11824  vsls-agent.exe
TCPv4  127.0.0.1:57949   127.0.0.1:57950       ESTABLISHED  13192  firefox.exe
TCPv6  [::1]:28473       [::1]:58231           ESTABLISHED  17180  leechagent.exe
TCPv6  [::1]:28473       [::1]:58228           ESTABLISHED  17180  leechagent.exe
...
```

### Example

The example shows the _sys/net_ directory and the ordinary non-verbose networking information.

[[resources/root_sysinfo_net.png]]

### For Developers
The _sys/net_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_net.c_ in the vmm project.

## Java API

Most functionality in the Memory Process File System is made available in a Java API for the use by developers. The Java API is a wrapper for the C/C++ API. Since the MemProcFS native library is 64-bit only the Java process must be running in 64-bit mode to be able to make use of the native library.

The Java API requires [Java Native Access (JNA)](https://github.com/java-native-access/jna) on the classpath.

The Java API is included in the form of `vmm.jar` in the pre-built version which should be included on the classpath.

API documentation in the form of [JavaDoc is available](https://ufrisk.github.io/javadoc/index.html).

### Example:

An example file containing a lot of use cases are found in the file [`VmmExample.java`](https://github.com/ufrisk/MemProcFS/blob/master/vmmjava/VmmExample.java). The Java API is similar to the Python API.

### Functionality:

The functionality mirror the functionality of the C/C++ API at large. Please consult the Java API sources or the C/C++ API documentation for information and guidance.

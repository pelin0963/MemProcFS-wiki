## The `search/bin` root and per-process directories

The directory _search/bin_ exists as a sub-directory to the file system root under _/misc/search/bin_ and in each process directory.

The search functionality allows for flexible efficient searching in process virtual memory and/or physical memory.

The files in the **search/bin** directories are listed in the table below:
| File                 | Description                                                                  |
| -------------------- | ---------------------------------------------------------------------------- |
| addr-min.txt         | Minimum address to search from.                                              |
| addr-max.txt         | Maximum address to search to.                                                |
| align.txt            | Byte-alignment of search (1, 2, 4, 8, 16, 0x1000 bytes etc.).                |
| readme.txt           | README file.                                                                 |
| reset.txt            | Write 1 to prepare for new search / abort on-going search / empty previous search. |
| result.txt           | Addresses with search matches.                                               |
| search.txt           | The search term in hexascii. Please see below for additional info.           |
| search-skip-bitmask.txt | Bitmask applied to each bit of search.txt.<br>0 = search normally. 1 = wildcard search of the specific bit. |
| status.txt           | Status of an ongoing or completed search.                                    |


Files in the _search/bin_ directories are read-write with the exception of _readme.txt_, _result.txt_ and _status.txt_.



### Search

Before a search is initiated it's possible to set optional constraints, such as min and max address, search alignment and any wildcard characters in the _search-skip-bitmask.txt_ bitmask.

The search is initiated by writing a hexascii term into the file _search.txt_. Once written the search starts immediately.

It's possible to follow the status of the search by monitoring _status.txt_. It's also possible to abort/clear a previous search by writing 1 into _reset.txt_ - this will allow for new search.



### Information

The search functionality in `misc/search` will search the entire physical memory space.

The search functionality in each process will search the process virtual memory. In case of a 64-bit address space the search will only be performed on the relevant address space (as shown in the [memmap](FS_Process_MemMap) functionality) for performance reasons. To search kernel address space please select the SYSTEM (PID 4) process or one of the CSRSS.EXE processes. Other processes are usually only searched in the user part of the address space - i.e. 0x0 to 0x7fffffffffff.



### Example

The example shows the search for pointer address references to the address `0xffffcf00d217eb40` in virtual address space of the System process. Please note that the byte order is reversed due to how pointers are stored in memory (little endian).

[[resources/proc_search.png]]



### For Developers
The _search/bin_ sub-directories are implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_search.c_ in the _vmm_ project.

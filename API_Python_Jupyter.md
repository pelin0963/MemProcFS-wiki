## MemProcFS in Jupyter Notebooks

The MemProcFS Python API is perfect for inclusion in your [Jupyter](https://jupyter.org/) notebooks.

MemProcFS memory forensics is blazingly fast when compared to some other commonly used tools. MemProcFS forensic mode generates CSV files which are perfect for inclusion in the Pandas data analytics framework.

Check out the MemProcFS Python API for information about the Python API itself.

A basic [example notebook](https://gist.github.com/ufrisk/66ab549ae85015891165124df46cb1e5) is available as a gist.

An demo video is available on YouTube.

<a href="https://www.youtube.com/watch?v=9lSuOrEiAP4" alt="Demo of MemProcFS Python API" target="_new"><img src="http://img.youtube.com/vi/9lSuOrEiAP4/0.jpg" height="350"/>

## The `search/yara` root and per-process directories

The directory _search/yara_ exists as a sub-directory to the file system root under _/misc/search/yara_ and in each process directory.

The search functionality allows for flexible efficient searching with yara rules in process virtual memory and/or physical memory.

The files in the **search/yara** directories are listed in the table below:
| File                 | Description                                                                  |
| -------------------- | ---------------------------------------------------------------------------- |
| addr-min.txt         | Minimum address to search from.                                              |
| addr-max.txt         | Maximum address to search to.                                                |
| readme.txt           | README file.                                                                 |
| reset.txt            | Write 1 to prepare for new search / abort on-going search / empty previous search. |
| result.txt           | Addresses with search matches.                                               |
| result-v.txt         | Detailed information about the yara search matches.                          |
| status.txt           | Status of an ongoing or completed search.                                    |
| yara-rules-file.txt  | Full path to yara rules file (compiled or source).                           |


Files in the _search/yara_ directories are read-write with the exception of _readme.txt_, _result.txt_ and _status.txt_.



### Search

Before a search is initiated it's possible to set optional constraints, such as min and max address.

The search is initiated by writing the path to a yara rules file into _yara-rules-file.txt_. Once written the search starts immediately if the rules can be loaded.

It's possible to follow the status of the search by monitoring _status.txt_. It's also possible to abort/clear a previous search by writing 1 into _reset.txt_ - this will allow for new search.



### Information

The search functionality in `misc/search` will search the entire physical memory space.

The search functionality in each process will search the process virtual memory. In case of a 64-bit address space the search will only be performed on the relevant address space (as shown in the [memmap](FS_Process_MemMap) functionality) for performance reasons. To search kernel address space please select the SYSTEM (PID 4) process or one of the CSRSS.EXE processes. Other processes are usually only searched in the user part of the address space - i.e. 0x0 to 0x7fffffffffff.



### Requirements
Yara searching requires supporting files - `vmmyara.dll` (Windows) and `vmmyara.so` (Linux). The required supporting files from the [vmmyara project](https://github.com/ufrisk/vmmyara) are pre-packaged with the MemProcFS binary releases.



### Example

The example shows the search for the trickbot malware in a single svchost process.

[[resources/proc_yarasearch.png]]



### For Developers
The _search/yara_ sub-directories are implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_searchyara.c_ in the _vmm_ project.

## The `conf` directory

The directory _conf_ exists as a sub-directory to the file system root.

The file contains global MemProcFS status and configuration settings not related to individual processes. The settings are exposed as files and are listed in the table below:


| File                                   | Type       | Description                                                       |
| -------------------------------------- | ---------- | ----------------------------------------------------------------- |
| cache_file_enable.txt                  | read/write | In-memory caching of memory pages when reading/writing memory.    |
| config_fileinfoheader_enable           | read/write | Enable info header in most file system info files.                |
| cache_paging_enable.txt                | read/write | Enable virtual memory - including Win10 compressed memory.        |
| config_printf_enable.txt               | read/write | Enable various informational texts in the console window.         |
| config_printf_v.txt                    | read/write | Enable verbosity level 1.                                         |
| config_printf_vv.txt                   | read/write | Enable verbosity level 2.                                         |
| config_printf_vvv.txt                  | read/write | Enable verbosity level 3.                                         |
| config_process_<br>_show_terminated.txt| read/write | Show terminated processes.                                        |
| config_refresh_enable.txt              | read       | Is automatic background refresh enabled?                          |
| config_refresh_force_all.txt           | write      | Write 1 to initiate a full refresh.                               |
| config_refresh_force_mem.txt           | write      | Write 1 to clear memory cache.                                    |
| config_refresh_force_tlb.txt           | write      | Write 1 to clear page table cache.                                |
| config_refresh_force_fast.txt          | write      | Write 1 to initiate a fast refresh (incl. partial process update).|
| config_refresh_force_medium.txt        | write      | Write 1 to initiate a medium refresh (incl. full process update). |
| config_refresh_force_slow.txt          | write      | Write 1 to initiate a slow refresh.                               |
| config_refresh_period_mem.txt          | read/write | Discard 33% of cached memory each _tick_period_ ms.               |
| config_refresh_period_tlb.txt          | read/write | Discard 33% of cached page tables each _tick_period_ ms.          |
| config_refresh_period_fast.txt         | read/write | Fast refresh (incl. partial process update) each _tick_period_ ms.|
| config_refresh_period_medium.txt       | read/write | Medium refresh (incl. full process update) each _tick_period_ ms. |
| config_refresh_period_slow.txt         | read/write | Slow refresh each _tick_period_ ms.                               |
| config_refresh_tick_period_ms.txt      | read/write | Refresh tick period in milliseconds (ms).                         |
| config_statistics_fncall.txt           | read       | Internal function call statistics.                                |
| config_symbol_enable.txt               | read/write | Enable statistic logging of function calls.                       |
| config_symbolcache.txt                 | read/write | Local debugging symbol cache directory.                           |
| config_symbolserver.txt                | read/write | Remote symbol server.                                             |
| config_symbolserver_<br>_enable.txt    | read/write | Enable Microsoft remote symbol server functionality.              |
| native_max_address.txt                 | read       | Max physical address of target system/memory acquisition device.  |
| statistics.txt                         | read       | Various internal statistics primarily related to memory management. |
| statistics_fncall.txt                  | read       | Internal function call statistics.                                |

#### Debugging symbols configuration
MemProcFS utilizes debugging symbols retrieved from .pdb files for some features. By default MemProcFS will load symbols from the Microsoft Symbol Server, after the user has accepted the EULA popup, into a local cache directory. The local cache directory resides by-default in the `\Symbols` sub-directory from the MemProcFS directory.

It is possible to configure non-default values of the local symbol cache directory and the symbol server by editing the files `config_symbolcache.txt` and `config_symbolserver.txt` in the `conf` directory. Valid non-default changes to these files will be persisted under the registry key `HKCU\Software\UlfFrisk\MemProcFS\` in the per-user registry hive.

It is possible to permanently disable symbols or the symbol server by DWORD values named `SymbolEnable` and `SymbolServerEnable` consisting of the value 0 under the above registry key. It is also possible to disable the symbol server at startup with the `-symbolserverdisable` startup option.

### Example

The example show the conf directory and its contents. The statistics file content is generated from various statistics - such as internal cache hits/misses. It is also possible to check and alter various settings. The example shows enabling verbosity by editing the file _config_printf_v.txt_ and looking at the maximum physical address of the target system.

[[resources/root_status.png]]

### For Developers
The _conf_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_conf.c_ in the _vmm_ project. In addition of being responsible for global status and configuration settings it's responsible for per-process status and configuration settings.
## The `phys2virt` root and per-process directories

The directory _phys2virt_ exists as a sub-directory to the file system root and in each process directory.

The phys2virt directory contains one special file named _phys.txt_ that the user of the Memory Process File System may write a physical address into. Once saved the page tables of the specific process (or all processes if in root directory) will be scanned for matching virtual addresses. Up to four (4) virtual addresses per process will be written to the _virt.txt_ file.

NB! Scanning the page tables of all processes for matching virtual addresses is an resource intense operation that may take time. Scanning live memory on a 4-core system with 200 active processes typically takes between 10-20 seconds.

NB! The scan is undertaken by walking all process page tables for active physical memory pages. Paged memory, even though otherwise available, will not be detected by this module.

* **phys.txt:** physical address in hex - **always user writable!**
* **virt.txt:** virtual addresses (in hex) that the virtual physical address maps to. If root module process identifier (PID) will also be displayed.

The _phys.txt_ file is writable. The _virt.txt_ file is read-only.

### Example

The example below shows how it is possible to map an interesting address in physical memory (0x1EB013000 in the example - red 🔴) to virtual addresses in process memory space.

Write the physical address into the file phys2virt/phys.txt (in the example - orange :orange_book:). The search may take some time. The result with up to four (4) matching virtual addresses per process will be shown together with the PID in the phys2virt/virt.txt file.

Optionally, the memory map of a process may be searched for the located virtual address (in the example it seems to be the PE/MZ header of Crypt32.dll - green :green_book:).

[[resources/root_phys2virt.png]]

### For Developers
The _phys2virt_ sub-directories are implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_phys2virt.c_ in the _vmm_ project.
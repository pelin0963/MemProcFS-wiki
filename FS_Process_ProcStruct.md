## The `py/procstruct` per-process directory

The directory _py/procstruct_ exists as a sub-directory in each process directory if the process is a Windows process.

The directory and its contents are implemented as a Python module. Python functionality must work for the _py/procstruct_ directory to be visible. Please find it in the _plugins/pym_procstruct_ relative to _MemProcFS.exe_.

The directory files contains the memory of the `EPROCESS` and `PEB` data structures related to the process being analyzed. If a 32-bit WoW64 process is analyzed files containing the memory of the 32-bit `PEB` will appear as `PEB32`. Both binary and test representations of memory are displayed.

The text files are always read-only. The binary files are writable if a write-capable memory acquisition device is used.

### Example

The example below shows the `EPROCESS`, `PEB` and `PEB32` of the 32-bit _OneDrive.exe_ process. The `EPROCESS` is viewed in a hex editor while the 32-bit `PEB32` is viewed in Notepad.

[[resources/proc_procstruct.png]]

### For Developers
The _py/procstruct_ sub-directory is implemented as a python module. The module is located in _plugins/pym_procstruct_ relative to _MemProcFS.exe_. The Python source code is well documented and make heavy use of the VmmPy library. For more information about Python modules check out the Python Modules wiki topic.
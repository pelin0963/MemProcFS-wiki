## The `sys` directory

The directory _sys_ exists as a sub-directory to the file system root.

The directory contains directories and files displaying various system information.


The files in the **sys** directory are listed in the table below:
| File              | Description                                                             |
| ----------------- | ----------------------------------------------------------------------- |
| computername.txt  | Name of the computer                                                    |
| time-boot.txt     | Time of system boot                                                     |
| time-current.txt  | Time of system                                                          |
| timezone.txt      | TimeZone of system                                                      |
| unique-tag.txt    | MemProcFS derived ID for the current memory image                       |
| version.txt       | Operating system version on format: major.minor.build                   |
| version-major.txt | Operating system major version                                          |
| version-minor.txt | Operating system minor version                                          |
| version-build.txt | Operating system build number                                           |

Files in the _sys_ directory and sub-directories are read-only.

### Example

The example shows the _sys_ directory, the computer name and operating system version.

[[resources/root_sysinfo2.png]]

### For Developers
The _sys_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys.c_ in the _vmm_ project.

## Rust API

Most functionality in the Memory Process File System is made available in a easy-to-use convenient Rust API for the use by developers. The Rust API is a wrapper around the native C/C++ API with some extras.

### Crate and API Documentation:

The MemProcFS Rust API is published as the **[memprocfs crate](https://crates.io/crates/memprocfs/) at crates.io** - which makes it very easy to include in your memory analysis projects!

Check out the **[API documentation](https://docs.rs/memprocfs/) at docs.rs**.

### Example projects:
Check out the
[example project](https://github.com/ufrisk/MemProcFS/blob/master/vmmrust/memprocfs_example/src/main.rs) and the 
[example MemProcFS plugin](https://github.com/ufrisk/MemProcFS/blob/master/vmmrust/m_example_plugin/src/lib.rs).


### Quick Examples:

```Rust
// Initialize MemProcFS on Linux targeting a live Windows system
// by reading memory using a PCILeech PCIe FPGA hardware device.
// After initialization list all processes.
let mut args = ["-printf", "-device", "fpga"].to_vec();
let vmm = Vmm::new("/home/user/memprocfs/vmm.so", &args)?
if let Ok(process_all) = vmm.process_list() {
    for process in &*process_all {
        println!("{} : {}", process.pid, process.info()?.name);
    }
}
```

```Rust
// Initialize MemProcFS on Windows - analyzing a memory dump file.
// Also trigger the forensic mode and scan for VMs.
// List all processes in the virtual file system directory /name/.
let mut args = ["-printf", "-forensic", "1", "-vm",
                "-device", "C:\\dumps\\memory.dmp"].to_vec();
let vmm = Vmm::new("C:\\MemProcFS\\vmm.dll", &args)?
if let Ok(vfs_all) = vmm.vfs_list("/name/") {
    println!("Number of files/directories: {}.", vfs_all.len());
    for vfs in &*vfs_all {
        println!("{vfs}");
    }
}
```

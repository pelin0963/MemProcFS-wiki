## The `forensic/ntfs` directory

The directory _forensic/ntfs_ exists as a sub-directory to the file system root.

The directory is hidden by default. It will appear once forensic mode has been started and processing is completed.

The directory contains a best-effort reconstructed file system reconstructed from NTFS MFT entries. If the files are small enough contents may reside within the NTFS MFT and may be recoverable by opening the file.

Folder 0 contains entries located in physical memory only. Folder 1 and above contains file systems ordered by number of entries. Usually folder 1 contains the system drive, but this is not guaranteed. Each file system root folder contains an **`$_ORPHAN`** directory containing files that could not be assigned to an existing directory.

The files in the **forensic/ntfs** directory are listed in the table below:
| File / Directory       | Description                                                   |
| ---------------------- | ------------------------------------------------------------- |
| ntfs_files.txt         | Summary of all files and folders.                             |
| 0                      | Directory containing physical-only MFT records.               |
| 1                      | Directory containing the largest file system (may not exist). |
| 2..N                   | Directories containing smaller file systems (may not exist).  |

Files in the _forensic/ntfs_ directory and its sub-directories are read-only.


### ntfs_files.txt:

The file **`ntfs_files.txt`** contains a complete record of the recovered NTFS MFT entities and their path. It's also possible to view the recovered files by clicking around in the sub-folders. Please note that the reconstruction is a best-effort reconstruction and may contain errors as well as missing files and folders.

The file `ntfs_files.txt` have the following format:
```
   # PHYS_ADDR RECID PARENT TIME_CREATE               TIME_MODIFY             SIZE FLAG PATH
==== ========= ===== ====== ======================= : ======================= ==== ==== ============
   0         0     5      0                     *** :                     ***    0 A  D \0
   1         0     0      5                     *** :                     ***    0 A  D \0\$_ORPHAN
   2  4d1e8800  729e    5eb 2018-04-11 23:34:43 UTC : 2018-08-01 22:44:22 UTC  455 A    \0\$_ORPHAN\2 - Search.lnk
   3  40f0e800 1bf82  1bf79 2018-08-01 19:34:38 UTC : 2018-08-01 19:34:38 UTC    0 A    \0\$_ORPHAN\8Ic32RWkZkc_1.bytecode
...
505f  b5839800 15392  152f7 2018-08-01 18:51:06 UTC : 2018-08-01 19:40:27 UTC    0 A  D \1\Users\CTF\Desktop
5060  25de2000 1552c  15392 2018-08-01 18:51:22 UTC : 2018-08-01 19:21:31 UTC  11a AR   \1\Users\CTF\Desktop\desktop.ini
5061   f7bec00 15c5b  15392 2018-08-01 18:53:16 UTC : 2018-08-01 18:53:16 UTC    0 A    \1\Users\CTF\Desktop\Microsoft Edge.lnk
5062 126515800 1caba  15392 2018-08-01 19:40:27 UTC : 2018-08-06 18:12:15 UTC    9 AR   \1\Users\CTF\Desktop\test.txt
5063 126515800 1caba  15392 2018-08-01 19:40:27 UTC : 2018-08-06 18:12:15 UTC   1d ARS  \1\Users\CTF\Desktop\test.txt:yes.txt
5064  b5839400 15391  152f7 2018-08-01 18:51:06 UTC : 2018-08-01 19:21:31 UTC    0 A  D \1\Users\CTF\Documents
5065  396ca800 15536  15391 2018-08-01 18:51:22 UTC : 2018-08-01 19:21:31 UTC  192 AR   \1\Users\CTF\Documents\desktop.ini
5066         0 153f5  15391 2018-08-01 18:51:11 UTC : 2018-08-01 18:51:11 UTC    0    D \1\Users\CTF\Documents\My Music
```

The flags are:<br>
**`A`** - Active.<br>
**`R`** - Resident (file data is recoverable).<br>
**`S`** - ADS (NTFS Alternative Data Stream).<br>
**`D`** - Directory.<br>

In the above example interesting lines are:

0: physical memory root directory<br>
1: physical memory orphan directory<br>
5062: file system 1 file test.txt which contains resident (recoverable) data.<br>
5063: file system 1 file test.txt ADS yes.txt which contains resident (recoverable) data.<br>
5066: file system 1 folder without a known physical address, most likely recovered from an index $I30 entry.<br>



### $_INFO folder data and metadata:

Each folder contains a special meta-data folder named `$_INFO`. The `$_INFO` folder contains a directory populated with metadata information about the NTFS record. The metadata files are:

| File        | Description                                             |
| ----------- | ------------------------------------------------------- |
| mftinfo.txt | Information in easy-to-read format about the MFT entry. |
| mftdata.mem | The 1024 byte long MFT record.                          |
| mftfile.bin | The file representation, if file and data recoverable.  |



### Example

The example shows browsing of the user desktop folder, as well as listings of the files in the file _ntfs_files.txt_.

[[resources/root_forensic_ntfs.png]]

### For Developers
The _forensic/ntfs_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_fc_ntfs.c_ in the vmm project.

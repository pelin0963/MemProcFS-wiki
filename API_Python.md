## Python API

**MemProcFS is available on Python pip!** `pip install memprocfs`

**If installing on Linux** supporting packages may be required: `sudo apt-get install make gcc pkg-config libusb-1.0 libusb-1.0-0-dev fuse libfuse-dev openssl libssl-dev lz4 liblz4-dev` Some functionality may be degraded on Linux. Please see the Linux section for more information.

Most functionality in MemProcFS is exported in a Python API. To make things easier the API is packaged in a pip package which is available as [`memprocfs`](https://pypi.org/project/memprocfs/) on Python PIP. This is also the preferred way of installing the Python package even though it's completely possible to compile and install locally or to run it from the MemProcFS folder.

If using the Python API outside the Python PIP package please note that Python may have to be started from the same folder as `vmmpyc.pyd` (Windows) or `vmmpyc.so` (Linux).

The Python pip package is a native binary CPython C Extension. It has been tested on 64-bit Windows and Linux (x64) as well as on 64-bit ARM (RPi4).

An introduction demo is available on YouTube.

<a href="https://www.youtube.com/watch?v=pLFU1lxBNM0" alt="Demo of MemProcFS Python API" target="_new"><img src="http://img.youtube.com/vi/pLFU1lxBNM0/0.jpg" height="350"/>



## Installing:

To install MemProcFS for Python please run:

`pip install memprocfs`

If successful, MemProcFS should now be installed and possible to use. Please note that a 64-bit Python installation is required. 32-bit Python installations will not work! Also for some functionality (such as remembering choices about debug symbols) it may be preferred to install in user-context rather than machine context; alternatively run MemProcFS as administrator the first time to persist the debug symbol choice.

It's also possible use MemProcFS for Python without installing the pip package. Please then start Python from the MemProcFS folder in which `vmmpyc.pyd` / `vmmpyc.so` resides.



## Overview:

The MemProcFS Python API is mostly built as a native CPython extension with minor Python components.

Please also see the guide entries about [base](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Base), [process](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Process) and [registry](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Registry#VmmRegKey) in the guide menu.

The components are roughly related to eachother as given in the tree below:

* `memprocfs` - package.
  * [`Vmm`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Base#Vmm) - base object.
    * `VmmMap` - info maps.
      * [`VmmVirtualMachine`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_VirtualMachine)
    * `VmmVfs` - virtual file system (vfs) info.
    * `VmmKernel` - kernel info.
    * `VmmPysicalMemory`
    * [`VmmProcess`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Process#VmmProcess)
      * `VmmMap` - process info maps.
      * `VmmVirtualMemory`
        * [`VmmScatterMemory`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_ScatterMemory)
      * [`VmmModule`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Process#VmmModule)
        * `VmmMap` - module info maps.
        * [`VmmPdb`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Base#VmmPdb) - PDB debug symbols.
    * [`VmmRegHive`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Registry#VmmRegHive)
      * `VmmRegMemory`
    * [`VmmRegKey`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Registry#VmmRegKey)
    * [`VmmRegValue`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Registry#VmmRegValue)
  * [`CONSTANTS`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Base#CONSTANTS) - MemProcFS constants.
  * [`RegUtil`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Registry#RegUtil) - registry utility functions.



## Getting Started with Examples:

Best way to getting started with examples is to have a look at the [`memprocfs_example.py`](https://github.com/ufrisk/MemProcFS/blob/master/files/memprocfs_example.py) file which contains a multitude of examples.

It's also possible to run a Python program at MemProcFS start-up with the `-pythonexec` parameter. This may be interesting in some live forensics scenarios. Please have a look at the example [`memprocfs_pythonexec_example.py`](https://github.com/ufrisk/MemProcFS/blob/master/files/memprocfs_pythonexec_example.py).

### Example #1 - Initialize from memory dump and read memory:

This example initializes a physical memory dump file for analysis. It then reads from physical memory as well as from process virtual memory.

```Python
import memprocfs

# Initialize base object vmm from dump file
vmm = memprocfs.Vmm(['-device', 'C:/Dumps/WIN10-X64-1909-18363-1.dmp'])

# read 0x20 bytes of physical memory from address 0x1000 and print it
# in hexascii on-screen.
print(vmm.hex( vmm.memory.read(0x1000, 0x20) ))

# retrieve the process object for 'explorer.exe'
process_explorer = vmm.process('explorer.exe')

# retrieve the module object 'kernel32' as seen by 'explorer.exe'
module_kernel32 = process_explorer.module('kernel32.dll')

# read 0x80 bytes from the base of 'kernel32' and print it as hexascii
virtual_address_kernel32 = module_kernel32.base
print(vmm.hex( process_explorer.memory.read(virtual_address_kernel32, 0x80) ))
```



### Example #2 - Extract registry autorun keys from FPGA PCIe DMA live memory:

Initialize from FPGA using PCIe DMA and query the live system for it's registry keys under HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run.

```Python
import memprocfs

# Initialize base object vmm from dump file
vmm = memprocfs.Vmm(['-device', 'fpga'])

# Retrieve the RUN registry key
regkey_run = vmm.reg_key('HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run')

# Loop over the autorun keys and print their values
for regvalue in regkey_run.values():
    print(regvalue.name + ':   ' + regvalue.vstr())
```



### Example #3 - Extract powershell event log from memory dump:

This example initializes a physical memory dump file for analysis. It then tries to locate the powershell event log and copy it to `c:\temp\powershell_eventlog_from_memory.evtx`. Files may be partially recovered from processes with open file handles. One of the svchost processes is responsible for event logging and will hold a handle to the powershell event log.

```Python
import memprocfs

# Initialize base object vmm from dump file
vmm = memprocfs.Vmm(['-device', 'C:/Dumps/WIN10-X64-1909-18363-1.dmp'])

# Iterate over all processes in the system
# if a svchost is found list its files under: 'files/handles'
for process in vmm.process_list():
    if process.name == 'svchost.exe':
        vfs_file_list = vmm.vfs.list('/pid/' + str(process.pid) + '/files/handles')
        for file_name in vfs_file_list:
            if 'PowerShell' in file_name and 'Operational' in file_name:
                file_path = '/pid/' + str(process.pid) + '/files/handles/' + file_name
                file_bytes = vmm.vfs.read(file_path, vfs_file_list[file_name]['size'])
                outfile = open('c:\\temp\\powershell_eventlog_from_memory.evtx', 'wb')
                outfile.write(file_bytes)
                outfile.close()
                print('powershell log extracted to: c:\\temp\\powershell_events.evtx')
```
## The `forensic/csv` directory

The directory _forensic/csv_ exists as a sub-directory to the file system root.

The directory is hidden by default. It will appear once forensic mode has been started and processing is completed.

The directory contains a comma separated (csv) files that may be used to import into Excel or Timeline Explorer. Timestamps are in UTC.

The CSV files are described in the table below:

| File                      | Description                                   |
| ------------------------- | --------------------------------------------- |
| devices.csv               | Device drivers.                               |
| drivers.csv               | Kernel drivers.                               |
| files.csv                 | Recoverable files.                            |
| findevil.csv              | Indicators of evil.                           |
| handles.csv               | Handles related to all processes.             |
| modules.csv               | Loaded modules information.                   |
| net.csv                   | Network connection information.               |
| process.csv               | Process information.                          |
| services.csv              | Services (user mode and kernel drivers).      |
| tasks.csv                 | Scheduled Tasks.                              |
| threads.csv               | Information about all threads on the system.  |
| timeline_all.csv          | Amalgamation of all timelines.                |
| timeline_kernelobject.csv | Kernel object manager objects.                |
| timeline_net.csv          | Network timeline.                             |
| timeline_ntfs.csv         | NTFS MFT timeline.                            |
| timeline_process.csv      | Process timeline.                             |
| timeline_registry.csv     | Registry timeline.                            |
| timeline_task.csv         | Scheduled Tasks timeline.                     |
| timeline_thread.csv       | Threading timeline.                           |
| timeline_web.csv          | Web timeline.                                 |
| unloaded_modules.csv      | Unloaded modules information.                 |
| virtualmachines.csv       | Virtual machines detected. (requires `-vm` startup option). |
| yara.csv                  | Summary forensic yara scan results.           |



### Example

The example shows looking at the _drivers.csv_ file in Timeline Explorer.

[[resources/root_forensic_csv.png]]



### For Developers
The _forensic/csv_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_fc_csv.c_ in the vmm project. Populating of the CSVs take place in the forensic sub-system and are spread out amongst different modules.

## The `memmap` per-process directory

The directory _memmap_ exists as a sub-directory in each process directory.

The _memmap_ directory contains two memory maps, one generated from the page tables the CPU uses to translate virtual memory into physical memory and one from the internal operating system virtual address descriptors. The _memmap_ directory also contains a sub-directory for verbose information about the vad map.

| File            | Description                                                                  |
| --------------- | ---------------------------------------------------------------------------- |
| pte.txt         | Memory map generated from the page tables.                                   |
| vad.txt         | Memory map generated from virtual address descriptors (VADs).                |

Files _pte.txt_ and _vad.txt_ are read-only.


### File: pte.txt

The file _pte.txt_ contains the hardware page table entries retrieved from the page table. The meaning of the different columns are as follows:

```
#       PID #pages        memory_address_range       rights  tag (module name)
===========================================================================
024e   1744      3 00007ff5bb3f1000-00007ff5bb3f3fff -r--
024f   1744      3 00007ff5bb40c000-00007ff5bb40efff -r--
0250   1744      1 00007ff6378f0000-00007ff6378f0fff -r--    mspaint.exe
0251   1744      2 00007ff6378f1000-00007ff6378f2fff -r-x    mspaint.exe
0252   1744      b 00007ff6378f5000-00007ff6378fffff -r-x    mspaint.exe
```


### File: vad.txt

The file _vad.txt_ contains the virtual address descriptors and their memory ranges. The meaning of the different columns are as follows:

```
#       PID  addr_vad_object   #pages     commit       memory_address_range        Type  Rights Tag
===================================================================================================
0029   8104 ffffa508508c6490       31        0 0 000001812ddc0000-000001812ddf0fff File  --r--- \Windows\System32\C_949.NLS
002a   8104 ffffa50854dc3670       10        8 0 000001812de00000-000001812de0ffff Heap  p-rw-- HEAP-01
002b   8104 ffffa508508c65d0       11        0 0 000001812de10000-000001812de20fff File  --r--- \Windows\System32\C_874.NLS
002c   8104 ffffa508508c7610       11        0 0 000001812de30000-000001812de40fff File  --r--- \Windows\System32\C_1258.NLS
002d   8104 ffffa508508c7890       31        0 0 000001812de50000-000001812de80fff File  --r--- \Windows\System32\C_936.NLS
002e   8104 ffffa508508c79d0        1        0 0 000001812de90000-000001812de90fff Pf    --r---
002f   8104 ffffa50854dc2db0       10        f 0 000001812dea0000-000001812deaffff Heap  p-rw-- HEAP-02
...
016c   8104 ffffa508508c6ad0       ee        d 0 00007ff6378f0000-00007ff6379ddfff Image ---wxc \Windows\System32\mspaint.exe
016d   8104 ffffa50851c14b40      16b       10 0 00007ffa9bcf0000-00007ffa9be5afff Image ---wxc \Windows\System32\mfc42u.dll
016e   8104 ffffa508508c83d0       54        4 0 00007ffa9d080000-00007ffa9d0d3fff Image ---wxc \Windows\System32\sti.dll
016f   8104 ffffa508508c77f0       d7        5 0 00007ffaa31f0000-00007ffaa32c6fff Image ---wxc \Windows\System32\efswrt.dll
```


### File: vad-v/_vad-v.txt

The file __vad-v.txt_ contains information about each page in each VAD about its virtual and physical address. The individual files contains the same information but on a per-VAD basis. The meaning of the different columns are as follows:

```
#         PID virtual_address  phys_addr    PTE             TP ACC VAD_addr         proto_phys   proto_PTE        Type  Rights Tag
==================================================================================================================================
...
00016f   6892 000002b6d66b4000 000026751000 8100000026751847 A rw- ffffdc0fe34e0ea0 000000000000 0000000000000000 Heap  p-rw-- HEAP-02                         
000170   6892 000002b6d66b5000 000026752000 8200000026752847 A rw- ffffdc0fe34e0ea0 000000000000 0000000000000000 Heap  p-rw-- HEAP-02                         
000171   6892 000002b6d66b6000 000026753000 8100000026753847 A rw- ffffdc0fe34e0ea0 000000000000 0000000000000000 Heap  p-rw-- HEAP-02                         
000172   6892 000002b6d66b7000 000026750000 8100000026750847 A rw- ffffdc0fe34e0ea0 000000000000 0000000000000000 Heap  p-rw-- HEAP-02                         
000173   6892 000002b6d66b8000 00012ab4f000 810000012ab4f847 A rw- ffffdc0fe34e0ea0 000000000000 0000000000000000 Heap  p-rw-- HEAP-02                         
000174   6892 000002b6d67a0000 00000099d000 920000000099d005 A r-- ffffdc0fe3476540 00000099d000 0a0000000099d921 File  --r--- \Windows\System32\locale.nls
000175   6892 000002b6d67a1000 0000de69c000 92000000de69c005 A r-- ffffdc0fe3476540 0000de69c000 0a000000de69c921 File  --r--- \Windows\System32\locale.nls
000176   6892 000002b6d67a2000 000000a9b000 9200000000a9b005 A r-- ffffdc0fe3476540 000000a9b000 0a00000000a9b921 File  --r--- \Windows\System32\locale.nls
000177   6892 000002b6d67a3000 000000f9a000 9200000000f9a005 A r-- ffffdc0fe3476540 000000f9a000 0a00000000f9a921 File  --r--- \Windows\System32\locale.nls
...
```


### Example

The example below shows the files _pte.txt_ and _vad.txt_. The file _pte.txt_ is generated by walking the actual page tables the CPU uses. It displays number of pages, address range, access rights and optionally which module the memory belongs to. The file _vad.txt_ is generated by the virtual address descriptors (VADs). Please note that the whole explorer.exe is one single VAD entry with access rights write/execute/copy.

[[resources/proc_memmap.png]]

### For Developers
The _memmap_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_proc_memmap.c_ in the _vmm_ project.
## The `sys/proc` directory

The directory _sys/proc_ exists as a sub-directory to the file system root.

The directory contains process related information in a convenient tree format.

The files in the **sys/proc** directory are listed in the table below:
| File            | Description                                                             |
| --------------- | ----------------------------------------------------------------------- |
| proc.txt        | Process tree - list processes and their parent processes in a tree view |
| proc-v.txt      | Process tree verbose - also list process image path and command line    |

Files in the _sys/proc_ directory are read-only.

### File: proc.txt
The file proc.txt contains a per-pid tree view of the known processes in the system. The view includes all processes including terminated ones.

#### Flags: 
`32` Process is 32-bit on 64-bit Windows.<br>
`E` Process is NOT found in EPROCESS list (memory corruption, drift or unlink)<br>
`T` Process is terminated<br>
`U` Process is user-account (non-system user)<br>
`*` Process is outside standard paths.<br>

```
   Process                  Pid Parent   Flag User      Create Time              Exit Time
--------------------------------------------------------------------------------------------------------
 - System                     4      0        SYSTEM    2020-08-01 19:20:20 UTC                      ***
 -- Registry                 88      4        SYSTEM    2020-08-01 19:20:10 UTC                      ***
 -- smss.exe                304      4        SYSTEM    2020-08-01 19:20:20 UTC                      ***
 -- MemCompression         1592      4        SYSTEM    2020-08-01 19:20:31 UTC                      ***
 - csrss.exe                396    388        SYSTEM    2020-08-01 19:20:24 UTC                      ***
 - wininit.exe              468    388        SYSTEM    2020-08-01 19:20:25 UTC                      ***
 -- services.exe            604    468        SYSTEM    2020-08-01 19:20:26 UTC                      ***
...
 -- userinit.exe           3996    564    TU  JohnSmith 2020-08-01 19:36:12 UTC  2020-08-01 19:40:09 UTC
 --- explorer.exe          4028   3996     U  JohnSmith 2020-08-01 19:20:58 UTC                      ***
 ---- mspaint.exe          1832   4028   E U  JohnSmith 2020-08-01 19:21:56 UTC                      ***
 ---- OneDrive.exe         8904   5896 32  U  JohnSmith 2020-08-01 19:21:56 UTC                      ***
...
```

### Example

The example shows the _sys/proc_ directory and the ordinary non-verbose process tree.

[[resources/root_sysinfo_proc.png]]

### For Developers
The _sys/proc_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_proc.c_ in the vmm project.

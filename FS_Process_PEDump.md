## The `pedump` per-process directory

The directory _pedump_ exists as a sub-directory in each process directory.

The _pedump_ directory contains best-effort reconstructed modules such as **.exe**, **.dll** and **.sys** files from memory fragments.

Please note that files in the _pedump_ directory are best-effort reconstructed files. The files may not match the real file system files since all fragments may not reside in memory and as such will be zero-padded. Even if most memory is available certain parts, such as the import table, of PE images that changed in runtime will not be re-constructed perfectly.

Files are writable if a write-capable memory acquisition device is used. Please note that physical memory backing modules are normally shared between all processes and that any writes may affect all processes with the module being written into.

### Example

The example below shows the files in the _pedump_ sub-directory of the _explorer.exe_ process. The directory contains best-effort reconstructed _.exe_ and _.dll_ PE modules. Missing information will be zero-padded if possible.

The modules are likely to have missing information and may not be executable as such. They may however be useful for other operations, such as looking at properties, reversing, debugging and scanning for known strings and signatures.

[[resources/proc_pedump.png]]

### For Developers
The _pedump_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _m_pedump.c_ in the _vmm_ project. The plugin does not provide an external API, but functionality may be accessed using the MemProcFS general filesystem APIs.
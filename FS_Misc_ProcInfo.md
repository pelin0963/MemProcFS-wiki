## The `misc/procinfo` directory

The directory _misc/procinfo_ exists as a sub-directory to the file system root. The directory exists on 64-bit systems only.

The directory contains various special aggregated process information that is only required in very specific circumstances.

The files in the **misc/procinfo** directory are listed in the table below:
| File                     | Description                                                                  |
| -------------------------| ---------------------------------------------------------------------------- |
| dtb.txt                  | Aggregated DTB/DirectoryTableBase information for all processes.             |
| progress_percent.txt     | Progress in analyzing memory to generate the resulting files in this module. |

Files in the _misc/procinfo_ directory is read-only.

### Information

The _misc/procinfo_ module aggregates potentially interesting speciality processes information and displays it. Generation may take some time and files will not be filled with content before _progress_percent.txt_ reach 100%.

### File: dtb.txt
The file dtb.txt contains a listing of DirectoryPageTable Roots (also known as DTB/CR3/PML4) and the related processes as displayed below. The line 0046 may indicate a stray DTB at physical address 0x1d91b0000. It is possible to manually try this value against potentially defunct processes by writing to the file dtb.txt in the process root folder (not recommended).

```
0000      4           1aa000 ffffe5070ee66040 System
0001    172           48c000 ffffe5070eef0080 Registry
0002    796          508a000 ffffe5071d029240 fontdrvhost.ex
0003   6992        10078c000 ffffe5071e9a4080 sihost.exe
0004   6224        102198000 ffffe5071ea0f080 taskhostw.exe
0005   7136        103e31000 ffffe5071e9db080 svchost.exe
0006   5756        105203000 ffffe5071e58b080 rundll32.exe
...
0043  12992        1c51a0000 ffffe50720454080 svchost.exe
0044  10044        1ca823000 ffffe50724fc0080 dllhost.exe
0045   3868        1d316c000 ffffe507217482c0 dllhost.exe
0046      0        1d91b0000                0 ---
0047   9200        1e3d38000 ffffe5071f5240c0 explorer.exe
0048   7044        202cfa000 ffffe50721481340 svchost.exe
...
```


### Example

The example shows the _misc/procinfo_ directory with DTBs related to processes and some potentially stray/unused DTBs.

[[resources/root_misc_procinfo.png]]

### For Developers
The _misc/procinfo_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_misc_procinfo.c_ in the vmm project.

## Building the Memory Process File System

### Windows:
MemProcFS is built with Visual Studio 2022. Please ensure that the C/C++ dev environment and the Visual Studio Python Tools are both installed.

MemProcFS targets both 64-bit and 32-bit platforms. 32-bit however not pre-built and not recommended. MemProcFS is however able to analyze memory of both 64-bit and 32-bit platforms.

### Linux:
MemProcFS is dependent on packages, before building please do a: `sudo apt-get install make gcc pkg-config libusb-1.0 libusb-1.0-0-dev fuse libfuse-dev libpython3-dev lz4 liblz4-dev`

MemProcFS is also available on the [LeechCore library](https://github.com/ufrisk/LeechCore). Clone leechcore and place it alongside MemProcFS. First build LeechCore. Then build MemProcFS binaries by typing `make` in the directories below:
* ` MemProcFS/vmm`
* ` MemProcFS/memprocfs`
* ` MemProcFS/vmmpyc`

# MemProcFS Remoting

MemProcFS Remoting allows for extremely efficient remote memory analysis and forensics over the network!

Open source memory forensics at scale in corporate active-directory settings are now a real possibility!

MemProcFS Remoting can be run in two different modes:
1. **Remote File System Mode** in which all memory analysis takes place on the remote target with a memory bandwith of GigaBytes/Second and only file system accesses are sent over the network. This is the recommended way to run MemProcFS Remoting.
2. **Local File System Mode** in which memory is pulled from the remote system over the network to the local system which performs all analysis. This is more network intense than the Remote File System Mode. This mode is limited by the network capacity.

MemProcFS Remoting is dependent on the [LeechAgent service](https://github.com/ufrisk/LeechCore/wiki/LeechAgent) being installed on the remote computer. If both the local and remote computer are part of the same Active Directory Domain and some minimal firewall openings (SMB - `tcp/445`) is open it's possible to remotely install the LeechAgent as a service on the remote computer. Once the LeechAgent service is installed it's possible to use it for MemProcFS remoting. For information about how to install LeechAgent see the [LeechAgent Install Wiki](https://github.com/ufrisk/LeechCore/wiki/LeechAgent_Install)

The user connecting must be an Administrator on the remote computer and run MemProcFS from an elevated command prompt locally. Only connections from administrators are allowed due to security reasons!


## Command Line arguments:

### `-remotefs`
Used in conjunction with the `-remote` command line option (see above). Force the memory analysis to be performed on the remote computer instead of on the local computer. Files (and not memory) are retrieved from the remote computer. It is recommended to add the `-remotefs` parameter to perform the memory analysis on the remote computer - maximizing performance and minimizing network load.



### `-remote`
Specified the protocol, authentication mechanism and the remote host (running the LeechAgent) to connect to. All connections will be encrypted and compressed.

`<protocol>://<authentication>:<hostname>`:`<logon>`

#### `<protocol>`
Protocol / connection mechanism to use:<br>
**`smb`** - connect over SMB named pipe over port `tcp/445`.<br>
**`rpc`** - connect over RPC port `tcp/28473` (which must be open in firewalls).

#### `<authentication>`
Authentication mechanism to use:
**`<kerberos-spn>`** - connect using Kerberos by specifying the Service Principal Name of the remote user running the LeechAgent. In the service mode this would be the remote computer account. If a computer is named `COMPUTER001` in the `CONTOSO.COM` Active Directory domain this would be `COMPUTER001$@CONTOSO.COM`.
**`ntlm`** - connect using NTLMv2. This is acceptable but a bit less secure than Kerberos since no mutual authentication takes place.
**`insecure`** - connect using no authentication. This is very insecure. LeechAgent does not allow this by default, but it may be optionally allowed only when LeechAgent is running in interactive mode (not as a service).

####  `<hostname>`
The remote host name or IP address.

####  `<logon>`
This parameter is optional and not recommended. Only use `logon` when connecting to a local computer administrator account using NTLM in non domain joined computers.


### Example: Install LeechAgent on a remote computer and perform MemProcFS forensics on remote computer at GigaBytes/Second:
1. Prerequisites
 * The local user is an administrator on the remote computer.
 * The local user have network access to the remote computer C$ drive over SMB - `tcp/445`
2. LeechAgent zip is downloaded from the [LeechCore releases](https://github.com/ufrisk/LeechCore/releases/latest/) section and unzipped.
3. Install LeechAgent as a service on the remote computer: `leechagent.exe -remoteinstall infectedcomputer.evilcorp.evil`. In this example the remote computer is named infectedcomputer and is part of the evilcorp.evil domain.
4. Connect with MemProcFS performing remote memory forensics utilizing the WinPMEM driver accessing memory at GBs/s. Connect over SMB named pipe `tcp/445`. Perform mutual authentication verifying the remote computers computer account (LeechAgent service is running as SYSTEM on the remote computer) using Kerberos.<br/> `memprocfs.exe -device pmem -remote smb://infectedcompute$@evilcorp.evil:infectedcomputer.evilcorp.evil -remotefs -forensic 1`

[[resources/memprocfs_remoting.png]]




### Example: Connect to a remote computer using RPC and NTLM, memory analysis on local computer:
1. Prerequisites
 * The remote computer allows connections to RPC- `tcp/28473`.
 * LeechAgent is already installed as a service (running as SYSTEM) on the remote computer.
2. Connect with MemProcFS performing remote memory analysis of the remote computer on the local computer (pulling all memory over the network). Connect over RPC `tcp/28473`. Use current users NTLM credentials to connect.<br/> `memprocfs.exe -device pmem -remote rpc://ntlm:remotecomputer.contoso.com`



### Example: Connect to a non-domain joined computer using NTLM, memory analysis on remote computer:
1. Prerequisites
 * The remote computer allows connections to SMB - `tcp/445`.
 * LeechAgent is already installed as a service (running as SYSTEM) on the remote computer.
2. Connect with MemProcFS performing remote memory forensics utilizing the WinPMEM driver accessing memory at GBs/s. Connect over SMB named pipe `tcp/445`. Use NTLM and ask for credentials for remote local administrator account.<br/> `memprocfs.exe -device pmem -remote smb://ntlm:192.168.1.35:logon -remotefs -forensic 1`

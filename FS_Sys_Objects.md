## The `sys/objects` directory

The directory _sys/objects_ exists as a sub-directory to the file system root.

The directory and its sub-directories contains information about named objects by the windows kernel object manager.

The files in the **sys/objects** directory are listed in the table below:
| File                           | Description                                               |
| ------------------------------ | --------------------------------------------------------- |
| objects.txt                    | Summary information about all objects.                    |
| ROOT/                          | Global Kernel Object Manager Root.                        |

Files in the _sys/objects_ directory and sub-directories are read-only.



### File: objects.txt

The file _objects.txt_ contains summary information about the named objects. The meaning of the different columns are as follows:

```
   # Object Address   Type          Description
-----------------------------------------------
0000 ffffbc0793417c40 Directory     \
0001 ffffbc0793416530 SymbolicLink  \\DosDevices  [\??]
0002 ffffbc0793416830 Directory     \\ObjectTypes
0003 ffffbc07934179c0 Directory     \\KernelObjects
0004 ffffbc079341b060 Directory     \\GLOBAL??
0005 ffffbc079341b6d0 Directory     \\Security
0006 ffffbc079341d060 Directory     \\Callback
0007 ffffbc07934204e0 SymbolicLink  \\SystemRoot  [\Device\BootDevice\Windows\]
0008 ffffbc0793420570 Directory     \\Device
...
```



### Example

The example shows the global kernel object hierarchy made available as a file system under the _sys/objects_ directory. It shows the object summary information as the file _objects.txt_ and the specific object and header of the driver _ad_driver_.

[[resources/root_sys_objects.png]]



### For Developers
The _sys/objects_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_obj.c_ in the vmm project.

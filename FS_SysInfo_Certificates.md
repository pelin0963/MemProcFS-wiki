## The `sys/certificates` directory

The directory _sys/certificates_ exists as a sub-directory to the file system root.

The directory contains process related information in a convenient tree format.

The files in the **sys/certificates** directory are listed in the table below:
| File                           | Description                                               |
| ------------------------------ | --------------------------------------------------------- |
| certificates/certificates.txt  | certificate information for all located certificates.     |
| certificates/[user]/*.cer      | certificate files.                                        |

Files in the _sys/certificates_ directory are read-only.

The directory exists only on Windows. Linux support is planned for the future.


### Example

The example shows the Local Machine certificate store inside the _sys/cert_ directory, a certificate and the file certificates.txt which contain a summary of all certificates successfully enumerated.

[[resources/root_sysinfo_certificates.png]]

### For Developers
The _sys/certificates_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_cert.c_ in the vmm project.

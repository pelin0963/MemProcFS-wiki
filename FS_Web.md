## The `forensic/web` directory

The directory _forensic/web_ exists as a sub-directory to the file system root.

The directory is hidden by default. It will appear once forensic mode has been started and processing is completed.

The directory contain web browser related information such as browser history from supported web browsers.

The files in the **misc/web** directory are listed in the table below:
| File                  | Description                                                             |
| --------------------- | ----------------------------------------------------------------------- |
| readme.txt            | General information about the web module.                               |
| web.txt               | Web browser history.                                                    |

Files in the _forensic/web_ directory are read-only.



### Supported web browsers

| Supported Browser     |
| --------------------- |
| Brave                 |
| Google Chrome         |
| Microsoft Edge (new)  |
| Mozilla Firefox       |

Internet Explorer, Microsoft Edge (old) and many other browsers are not supported. Chromium-based browsers not listed may sometimes be detected as Chrome.



### Recoverable events
The web module may recover, by using a best-effort algorithm, events related to:

| Type       | Description      |
| ---------- | ---------------- |
| `VISIT`    | Page visit.      |
| `DOWNLOAD` | File download.   |
| `LOGINPWD` | Saved login data. (The actual password is not recovered by MemProcFS but is often recoverable manually). |

The web browser contains many more events, such as cookies, which is not parsed by MemProcFS. Events may be recovered from active and recently closed web browsers.



### Example

The example shows the _forensic/web_ directory with the files _readme.txt_ and _web.txt_. The _web.txt_ contains web browser information.

[[resources/root_misc_web.png]]



### For Developers
The _forensic/web_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_fc_web.c_ in the _vmm_ project.

## The `minidump` per-process directory

The directory _minidump_ exists as a sub-directory in each process directory.

The _minidump_ directory contains a WinDbg compatible full process minidump.dmp file that may be used for debugging.

The minidump file contains:
 - process memory including stacks, heaps and PE images.
 - active process thread information including some CPU register information.
 - active process modules (.dll/.exe).

Minidump files are reconstructed on a best-effort basis. Process memory may be valid, missing or zero-padded depending whether the backing memory is available or inaccessible due to paging.

Minidump files are only generated if certain prerequisites are met:
 1) The process must be an active user-mode process. Special "kernel" processes such as System, Registry and MemCompression won't have minidump files generated.
 2) If debug symbols from the Microsoft symbol server is missing certain functionality will be missing (Threads). The minidump will however still be generated.

The minidump.dmp file is read-only.

### Example

The example below shows the files _minidump.dmp_ and _readme.txt_ for the _explorer.exe_ process. The file _minidump.dmp_ is open directly in WinDbg for a debugging session showing some information about the callstack and CPU registers related to one of the threads.

[[resources/proc_minidump.png]]

### For Developers
The _minidump_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_proc_minidump.c_ in the _vmm_ project.
## The `misc/view` directory

The directory _misc/view_ exists as a sub-directory to the file system root.

The directory contains two special views of the main file system.

### `txt` view

The directory _misc/view/txt_ and sub-directories exists as a mirror to the root file system - but only show text files. This may ease broad searches in text info-files.

### `bin` view

The directory _misc/view/bin_ and sub-directories exists as a mirror to the root file system - but only show non-text files smaller than 128MB. This may ease some activities - such as anti-virus scans.



### For Developers
The _misc/view_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_misc_view.c_ in the _vmm_ project.


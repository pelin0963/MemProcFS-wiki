## The `vmemd` per-process directory

The directory _vmemd_ exists as a sub-directory in each process directory.

The directory contains files of virtual memory according to the process memory map. Each file maps towards a memory map entry. Primarily entries from the virtual address descriptor (VAD) memory map are used. If a memory entry exists in the hardware page table map (PTE) but not in the VAD memory map it will also be displayed.

A file may consist of one or more contiguous virtual memory pages. Please note that often not all pages are allocated towards physical pages. They may also be unmapped, mapped towards a page file or mapped towards compressed virtual memory. If a page is unreadable it will be zero padded.

The Memory Process File System will use tag information in the memory map, such as module/.dll name, and include it in file names should such information exist.

The files in the _vmemd_ directory does not allow read/write past the end of file even if virtual memory with different page permissions should exist contiguously in virtual memory after the end of the file.

Files are writable if a write-capable memory acquisition device is used.

### Example

The example below shows hex editing of the file _0x00007ff750930000-explorer.exe.vvmem_ - when looking at the process memory map file the hex edited file maps perfectly towards the virtual memory page containing the PE(MZ) header of the cmd.exe module in the cmd.exe process.

[[resources/proc_vmemd_2.png|hexediting a virtual memory file in the vmemd directory.]]

### For Developers
The _vmemd_ sub-directory is implemented as a separate native C-code plugin. The plugin _m_vmemd.dll_ is located in the plugins directory that exists as a sub-directory to the directory of _MemProcFS.exe_. Well documented source code exists in the _m_vmemd_ project in the Visual Studio solution.
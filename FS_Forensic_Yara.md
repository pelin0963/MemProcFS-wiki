## The `forensic/yara` directory

The directory _forensic/yara_ exists as a sub-directory to the file system root.

The directory is hidden by default. It will appear once forensic mode has been started and processing is completed if optional yara rules have been specified.

The directory contains results of a forensic yara scan of process and kernel virtual address spaces.

The forensic scan is conducted with rules specified in the start-up option `-forensic-yara-rules`. The rules may be either compiled rules or source rules (including index rules referencing other rules). Example: `memprocfs.exe -device c:\dumps\win10.raw -forensic-yara-rules c:\yara\rules\windows_malware_index.yar`

The files in the **forensic/yara** directory are listed in the table below:

| File                      | Description                                   |
| ------------------------- | --------------------------------------------- |
| match-count.txt           | The number of yara matches.                   |
| result.txt                | Detailed yara match information.              |
| rules.txt                 | The user-defined rules used in the scan.      |



### Example

The example shows looking at forensic yara matches which indicates Trickbot in the svchost.exe process.

[[resources/root_forensic_yara.png]]



### For Developers
The _forensic/yara_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_fc_yara.c_ in the vmm project.

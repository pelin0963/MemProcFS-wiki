## The `misc` directory

The directory _misc_ exists as a sub-directory to the file system root.

The directory contains sub-directories representing various miscellaneous analysis tasks related to the global context.

For more information about each _misc_ analysis task please consult the guide item for each individual analysis task.

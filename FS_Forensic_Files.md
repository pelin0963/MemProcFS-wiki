## The `forensic/files` directory

The directory _forensic/files_ exists as a sub-directory to the file system root.

The directory is hidden by default. It will appear once forensic mode has been started and processing is completed.

The directory contains a best-effort reconstructed file system reconstructed from potentially recoverable file objects in the kernel. The file objects are retrieved from the kernel pool and process handles.

The file _files.txt_ contains a complete record of the recovered files and their path. It's also possible to view the recovered files by clicking around in the sub-folders. Please note that the reconstruction is a best-effort reconstruction and may contain errors as well as missing files and folders.

Also see the [NTFS MFT](https://github.com/ufrisk/MemProcFS/wiki/FS_Forensic_Ntfs) file recovery module - which may recover small files not visible in this module.

| File        | Description                                             |
| ----------- | ------------------------------------------------------- |
| readme.txt  | README file.                                            |
| files.txt   | Files recovered                                         |

The file _files.txt_ have the following format:
```
   # OBJECT_ADDRESS   TYPE     SIZE NAME                     PATH
=================================================================    
0000 ffff8e03a085a830 Data     2560 svchost.exe.mui          \Windows\System32\en-US\svchost.exe.mui
0001 ffff8e03a0a96670 Data    17920 oleaccrc.dll.mui         \Windows\System32\en-US\oleaccrc.dll.mui
0002 ffff8e03a0a9a1d0 Data     5632 twinui.pcshell.dll.mui   \Windows\System32\en-US\twinui.pcshell.dll.mui
...
01b0 ffff8e03a7809950 Cache 1118208 System.evtx              \Windows\System32\winevt\Logs\System.evtx
01b1 ffff8e03a780a120 Cache 5312512 Security.evtx            \Windows\System32\winevt\Logs\Security.evtx
01b2 ffff8e03a780a2b0 Cache 1118208 Windows PowerShell.evtx  \Windows\System32\winevt\Logs\Windows PowerShell.evtx
...
```

Files in the _forensic/files_ directory and its sub-directories are read-only.



### Example

The example shows browsing of the recovered file system, as well as listings of the files in the file _files.txt_.

[[resources/root_forensic_files.png]]

### For Developers
The _forensic/files_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_fc_file.c_ in the vmm project.

## The `sys/users` directory

The directory _sys/users_ exists as a sub-directory to the file system root.

The directory contains information about the users on the system.

The files in the **sys/users** directory are listed in the table below:
| File              | Description                                                                            |
| ----------------- | -------------------------------------------------------------------------------------- |
| net/users.txt     | Information about the users on the system.                                             |

Files in the _sys/users_ directory are read-only.

### File: net/users.txt

```
   # Username                         SID
-----------------------------------------
0000 SANSDFIR                         S-1-5-21-1552841522-3835366585-4197357653-1001
...
```

### Example

The example shows the _sys/users_ directory and the file users.txt

[[resources/root_sysinfo_users.png]]

### For Developers
The _sys/users_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_user.c_ in the vmm project.

## The `Process Base` per-process directory

All processes that are identified are listed in an individual directory shown by name in the `/name/` directory and by PID in the `/pid/` directory.

Each process directory contains both files and sub-directories related to the process. Sub-directories are implemented as separate plugin/modules. Please check the individual documentation for each sub-directory for their function.

The files and their functions are listed in the table below:

| File                | Description                                                                                  |
| ------------------- | -------------------------------------------------------------------------------------------- |
| dtb.txt             | Directory table base in physical address space. By default the kernel DTB will be used. <br/> File is **writable** should the user wish to override the DTB/CR3 (not recommended).         |
| dtb-kernel.txt      | Kernel-mode directory table base in physical address space. (shown if different from dtb.txt)|
| dtb-user.txt        | User-mode directory table base in physical address space.                                    |
| memory.vmem         | The virtual memory as a file.                                                                |
| name.txt            | Name of the process - max 15 characters.                                                     |
| name-long.txt       | Complete name of the process.                                                                |
| pid.txt             | Process IDentifier (PID).                                                                    |
| ppid.txt            | Parent process identifier.                                                                   |
| state.txt           | EPROCESS state of the process (0 = active).                                                  |
| time-create.txt     | Process creation time.                                                                       |
| time-exit.txt       | Process exit time (does not exist on active processes).                                      |
| win-cmdline.txt     | Command line.                                                                                |
| win-curdir.txt      | Current directory.                                                                           |
| win-environment.txt | Environment variables.                                                                       |
| win-eprocess.txt    | Virtual address of the EPROCESS struct.                                                      |
| win-path.txt        | Kernel path of the process executable.                                                       |
| win-peb.txt         | Process Environment Block (PEB).                                                             |
| win-peb32.txt       | 32-bit Process Environment Block (PEB) if existing.                                          |
| win-title           | Window title of the application.                                                             |

The memory map only contain user-mode accessible memory for normal processes. Kernel memory mapped into the processes are filtered out for normal processes. The most noteworthy exception is the Windows System process (PID 4) which displays kernel memory.

The virtual memory file `memory.vmem` is an 1:1 mapping between file and user-accessible virtual memory. In the 64-bit x64 memory model memory is located between virtual addresses `0x0-0x7fff'ffffffff`. Non existent memory mappings are null/zero-byte padded. Kernel memory is traditionally put into the range `0xffff8000'00000000-0xffffffff'ffffffff` - with the topmost 16 bits set to `0xffff`. Due to file system limitations it's not possible to display this large files on Windows. To analyze kernel memory in the System process it's possible to just remove the top 16-bits (0xffff) from the virtual address and have a look int he file for the correct contents. In the 32-bit x86 and PAE memory models memory are shown in the range `0x0-0xffffffff`. Virtual memory may not always map to physical memory - it may also map to page files and compressed memory.

The win-eprocess.txt file shows the address of the EPROCESS structure in virtual memory. Since EPROCESS is a kernel structure it's not displayed by the Memory Process File System in the process memory map or memory.vmem file for the specific process - even though it may exist in it. To view the EPROCESS in the memory.vmem file please address the memory.vmem file of the System (4) process.

All files in the process base directory are read-only with the exception of the _dtb.txt_ and _memory.vmem_ files. The _memory.vmem_ file is writable if a write-capable memory acquisition device is used.

### Example

The example below shows the process directory of _explorer.exe_ with PID 1844. The _win-peb_ file containing the virtual address of the process environment block is shown in notepad. The _vmem_ file is hex edited at the base of the PEB of _explorer.exe_ in the hex editor.

[[resources/proc_base_2.png]]

### For Developers
The process directories are part of the vmm project core. All sub-directories are however implemented as plugins/modules in order to be able to easily extend the Memory Process File System. Plugins may be implemented as native C plugins or as embedded Python plugins. Please consult separate documentation for more information.
## MemProcFS `command line` options

This guide describes the various command line options that is possible to start MemProcFS.exe with. In addition to starting MemProcFS with command line options it's also possible to start it by double clicking on memory dumps and select open with MemProcFS.

MemProcFS uses the [LeechCore](https://github.com/ufrisk/LeechCore) library for physical memory acquisition and alteration. MemProcFS forwards LeechCore related command line options, primarily related to physical memory acquisition devices, to the LeechCore library. Find a short description about such options primarily the [`-device`](#-device) option in the LeechCore section below.



<hr>

### `-arch`
CPU / memory architecture. `-arch` is auto-detected. When analyzing raw arm64 memory dumps specify: `-arch arm64`.


<details>

| arch   | description       |
| ------ | ----------------- |
| x86    | 32-bit x86 paging. Older 32-bit Windows running on Intel/AMD CPUs. |
| x86pae | 32-bit x86 paging with physical address extension. 32-bit Windows running on Intel/AMD CPUs. |
| x64    | 64-bit x64 paging. Most common option. Intel/AMD CPUs. |
| arm64  | 64-bit arm paging. ARM CPUs such as Apple Silicon. |

</details>



<hr>

### `-debug-pte-quality-threshold`
Debug option. Set PTE quality threshold. May increase results on the expense of quality. Default = 32.
<details>
MemProcFS employs a quality check on page table entries (used in virtual to physical memory translation) in 64-bit mode. The quality check will try to filter away bad page tables to increase quality by default. In some cases it may filter too much. By default it will allow 32 bad entries before marking the page table as bad.

#### Example:
Completely disable PTE quality checks:<br>
`-debug-pte-quality-threshold 512`
</details>



<hr>

### `-disable-infodb`
Disable usage and symbol lookup of cached symbols from the locally stored info.db. This also disables built-in Yara rules and some other functionality relying on `info.db`.
<details>
Disable any lookups from the `info.db` file. This will have the same effect as deleting the `info.db` file for the MemProcFS instance.

Symbol lookups via the Microsoft symbol subsystem may still work (disable with `-disable-symbols`).

Built-in Yara rules will also stop working.

</details>



<hr>

### `-disable-python`
Disable all Python functionality.
<details>
MemProcFS allows for Python plugin functionality. If this is not desirable it's possible to disable loading of the Python plugin sub-system by supplying the `-disable-python` command line argument.

#### Example:
Disable the use of Python plugins:<br>
`-disable-python`
</details>



<hr>

### `-disable-symbols`
Disable symbol lookup from debug symbols.
<details>
Disable the debug symbol subsystem. This will prevent downloading of debug symbols from Microsoft servers (`-disable-symbolserver`) as well as the usage of already locally stored symbols in the form of cached .pdb files.

Any symbols still possible to look-up from info.db will still work. Disable info.db symbol lookup with `-disable-infodb`.

#### Note:
Debug symbols downloaded from Microsoft are not yet supported on Linux.
</details>



<hr>

### `-disable-symbolserver`
Disable automatic downloading of debug symbols from Microsoft.
<details>
By default MemProcFS tries to download symbols from the Microsoft Symbol Server. Symbols are required for the MemProcFS .pdb debug symbol sub-system which some MemProcFS is dependent on. If one do not wish to automatically download symbols from the Microsoft Symbol Server it's possible to specify the `-disable-symbolserver`.

Symbols already in the local cache will still be loaded, but no connections towards the Microsoft Symbol Server will take place when the `-disable-symbolserver` command line option is specified.

Please also note that the 1st time MemProcFS is started the user is asked to accept an EULA for the Microsoft Symbol Server. If the user answers no a file __symsrv.no__ will be created and the symbol server will be permanently disabled until the __symsrv.no__ file is deleted manually from the MemProcFS directory.

#### Note:
Debug symbols downloaded from Microsoft are not yet supported on Linux.
</details>



<hr>

### `-disable-yara`
Disable the Yara scanning sub-system and all Yara related functionality.



<hr>

### `-disable-yara-builtin`
Disable built-in Yara rules - but allow user-initiated Yara scans to remain intact.



<hr>

### `-f`
Specify file / memory acquisition device. See [`-device`](https://github.com/ufrisk/MemProcFS/wiki/_CommandLine#-device) for additional information.



<hr>

### `-forensic`
Enable forensic mode by command line (recommended way to run forensic mode). Example: `-forensic 2`

<details>

Enable forensic mode by command line. The option takes a 2nd argument which is the forensic mode detailed below.

When forensic mode is enabled command line and the analysis takes place on a file the forensic results should be reproducible between runs on the same MemProcFS versions. If not enabled on startup (enabled in the file system) multi-threading and read-caches may result in slightly different results due to ordering. When doing forensic processing it's recommended to enabled the forensic mode via command line.

| Value | Forensic mode                                          |
| ----- | ------------------------------------------------------ |
| 1     | In-memory only SQLITE database.                        |
| 2     | Temporary SQLITE database deleted upon MemProcFS exit. |
| 3     | Temporary SQLITE database kept upon MemProcFS exit.    |
| 4     | Well known SQLITE database kept upon MemProcFS exit.   |

#### forensic mode virtual machine support:
Forensic mode will also enable the detection of virtual machines akin to the `-vm` option. To enable forensic modes inside any detected Windows virtual machines please use the option `-vm-nested`
</details>


<hr>

### `-forensic-process-skip`
List of comma-separated short process names (max 15 chars/process) to skip in forensic yara and findevil scans in addition to default skip list. Allows skipping problematic processes such as anti-virus processes that may contain signatures resulting in false positives.

Example: `-forensic-process-skip "AvastSvc.exe,navapsvc.exe"`



<hr>

### `-forensic-yara-rules`
Yara rules file. The rules file may contain compiled rules or source rules (including index rules referencing other rules).

Example: `-forensic-yara-rules C:\rules\malware\MALW_TrickBot.yar`



<hr>

### `-license-accept-elastic-license-2-0`
Accept the [Elastic License 2.0 License agreement](https://www.elastic.co/licensing/elastic-license) required for built-in Yara rules used by forensic FindEvil functionality. Acceptance is cached and only has to be given for the first forensic run.
<details>
Forensics mode make use of Yara rules from [Elastic Security](https://www.elastic.co/) for additional FindEvil functionality. The rules require acceptance of the [Elastic License 2.0 License agreement](https://www.elastic.co/licensing/elastic-license) for usage. The license agreement only needs to be accepted once. Not accepting the license agreement will disable Yara scanning with rules from Elastic Security.

The Elastic License 2.0 applies to some built-in FindEvil yara rules. The Elastic License 2.0 does not apply to MemProcFS itself.

</details>



<hr>

### `-logfile`
Logfile. Use in conjunction with `-loglevel`. Example: `-logfile c:\temp\memprocfslog.txt`.



<hr>

### `-loglevel`
Logging level. `-loglevel` is a comma-separated list of logging options. `-loglevel` is more fine grained than `-v` and `-vv`.


<details>

| loglevel | description      |
| -------- | ---------------- |
| 0        | off / no logging |
| 1        | critical         |
| 2        | warning          |
| 3        | info / normal    |
| 4        | verbose          |
| 5        | debug            |
| 6        | trace            |
| 7        | all              |

Each loglevel item may be specified as either:

* `[loglevel]` = apply [loglevel] to all modules.
* `[module]:[loglevel]` = apply [loglevel] to specified [module].
* `f:[module]:[loglevel]` = apply [loglevel] to specified [module] and log its output to file specified in `-logfile` option.

#### Examples: 
* `-loglevel 4` = Set default log level to verbose (4)
* `-loglevel 4,vmm:7` = Set default log level to verbose (4) and loglevel of the VMM module to all.
* `-loglevel f::4,f:registry:7` = Enable verbose file logging to file specified in `-logfile <file>` option except for registry module which will log all to file.

</details>



<hr>

### `-mount`
MemProcFS tries to mount as drive letter __M:__ by default on Windows. MemProcFS may mount at other drive letters as specified by the `-mount` option. On Linux the `-mount` option must be given with the path to mount the fuse file system on. 

#### Example (Windows):
To mount MemProcFS as drive letter __Q__ specify command line option:<br>
`-mount Q`

#### Example (Linux):
To mount MemProcFS at __/home/pi/mnt__ specify command line option:<br>
`-mount /home/pi/mnt`



<hr>

### `-norefresh`
If live memory is captured either via driver or PCIleech FPGA device MemProcFS will auto-detect this and do background updates to update process listings and other information. Background refreshes are done to keep data up-to-date and to provide a good user experience (= fast and responsive with relevant current data).

Background refreshes take up small amounts of system resources and may not always be desirable. It's possible to disable background refreshes by specifying the `-norefresh` command line option. Please note that it's usually not recommended to set this option. This option have no meaning if static memory such as a memory dump file is analyzed.



<hr>

### `-pagefileX`
Windows use page files to page out memory not frequently used to disk. Page files are in a state of flux and are usually not recommended to use for memory analysis unless they are acquired at the same time, or at the very least very close in time, to the memory dump file acquisition.

On a modern Windows 10 system each page file is given an index number. On a default configured Windows 10 system _pagefile.sys_ will get index 0 while _swapfile.sys_ will get index 1. On a non-default configured system with less or more page files this may differ.

To use page files with MemProcFS please use the `-pagefileX` command line option - where `X` is the page file index number. Please note that if old page files taken at different points time as compared to the memory dump are used then the memory analysis will be severely degraded by bad data.

#### Example:
Use _pagefile.sys_ and _swapfile.sys_ to increase memory analysis quality of a Windows 10 memory dump:<br>
`-pagefile0 pagefile.sys -pagefile1 swapfile.sys`



<hr>

### `-pythonexec`
Execute a Python batch program at MemProcFS startup.
<details>

MemProcFS allows execution of a Python program at startup. The Python program is run inside the MemProcFS [Python API](https://github.com/ufrisk/MemProcFS/wiki/API_Python) context.

If the `-pythonexec` option is specified MemProcFS will not mount the virtual file system at startup unless the `-mount` option is explicitly specified. This makes it a perfect way to perform headless command line memory forensics.

If forensic mode is activated by command line the Python program will run only after the forensic mode processing is completed. This may take some time.

An example use case would be to copy MemProcFS with winpmem_x64.sys and an embedded Python installation in the Python sub-directory to a remote host to perform fast physical memory forensics in batch mode without having to install the Dokany file system driver.

The complete MemProcFS Python API is available in the Python execution environment. MemProcFS is already initialized in the object [`vmm`](https://github.com/ufrisk/MemProcFS/wiki/API_Python_Base#Vmm). The vmm object may be used but should not be closed.

An example program is found in the file: `memprocfs_pythonexec_example.py`.

#### Example:
Execute the Python program _memprocfs_pythonexec_example.py_ after forensic mode is completed and without mounting a file system:<br>
`memprocfs -forensic 1 -pythonexec memprocfs_pythonexec_example.py`

Execute the Python program _memprocfs_pythonexec_example.py_ before mounting the file system as M:<br>
`memprocfs -mount m -pythonexec memprocfs_pythonexec_example.py`
</details>



<hr>

### `-pythonpath`
Specify a custom path to the Python installation to be used.
<details>
MemProcFS allows for Python plugin functionality. Examples of such plugins are the _procstruct_ plugin showing _EPROCESS_ and _PEB_ in a convenient format and the 3rd party _pypykatz_ plugin providing mimikatz-style functionality to MemProcFS. In order to run Python plugins MemProcFS needs access to a Python installation.

By default MemProcFS will use the 64-bit Python 3.6 or later installation found on the current PATH. If no Python installation is found on the PATH MemProcFS will try load a Python environment from its Python sub-directory.

If loading of a Python installation fails or one wish to load a separate Python installation please use the `-pythonpath` command line option.

#### Example:
Use a non-default 64-bit Python installation at _C:\Program Files\Python37_:<br>
`-pythonpath C:\Program Files\Python37`

#### Note:
The `-pythonpath` command line option is not supported on Linux. The default Python3 installation will be used.
</details>



<hr>

### `-v` `-vv` `-vvv`
Verbosity level. By default MemProcFS only print a select few info messages in the console window.

<details>

The `-v` option enables verbosity.

The `-vv` option enables extra verbosity.

The `-vvv` option enables extra extra verbosity and will generate a large amount of data including all memory read/written to/from files or devices. The `-vvv` option is not recommended.
</details>



<hr>

### `-vm`
Enable detection of supported [virtual machines](https://github.com/ufrisk/MemProcFS/wiki/VM).
<details>
VM detection is resource intense and is only supported on 64-bit MemProcFS builds. Detected VMs are mounted either as physical memory or as a full MemProcFS file system under the ` /vm/` root directory. Detection of nested VMs (additional VMs inside the detected VM) will be disabled.
</details>



<hr>

### `-vm-basic`
Enable detection of supported [virtual machines](https://github.com/ufrisk/MemProcFS/wiki/VM) as physical memory only.
<details>
VM detection is resource intense and is only supported on 64-bit MemProcFS builds. Detected VMs are mounted as physical memory under the ` /vm/` root directory.
</details>



<hr>

### `-vm-nested`
Enable detection of supported [virtual machines](https://github.com/ufrisk/MemProcFS/wiki/VM) included nested virtual machines.
<details>
VM detection is resource intense and is only supported on 64-bit MemProcFS builds. Detected VMs are mounted either as physical memory or as a full MemProcFS file system under the ` /vm/` root directory. Detection of nested VMs (additional VMs inside the detected VM) will be enabled.
</details>



<hr>

### `-waitinitialize`
To decrease startup time tasks, such as starting the .pdb debug symbol sub-system, is started asynchronously after system startup. This may lead to certain functionality being unavailable for the first seconds after startup. This may in some cases have undesirable effects - such as missing data and limited memory recovery rates for the first seconds after startup.

If it's desirable to have full functionality available immediately after MemProcFS startup please specify the `-waitinitialize` command line option. Startup times may increase somewhat.



<hr>

### `-z`
Specify file / memory acquisition device. See [`-device`](https://github.com/ufrisk/MemProcFS/wiki/_CommandLine#-device) for additional information.



<hr>

## LeechCore command line options:

MemProcFS uses the [LeechCore](https://github.com/ufrisk/LeechCore) library for physical memory acquisition and alteration. MemProcFS forwards LeechCore related command line options, primarily related to physical memory acquisition devices, to the LeechCore library.

For a full detailed description of [LeechCore](https://github.com/ufrisk/LeechCore) related command line options and per-device type information please consult the [LeechCore Wiki](https://github.com/ufrisk/LeechCore/wiki).

A brief overview of LeechCore related command line options are given below:

### `-device`
MemProcFS/LeechCore supports a number of different memory acquisition "devices".

Supported acquisition methods range from dump files [raw dump files, Microsoft full crash dump files, Elf full core dump files (virtualbox) to driver-assisted capture of live memory [DumpIt or WinPMEM] to hardware PCILeech FPGA devices.

For more detailed information about each memory acquisition device and also about additional supported "devices" check out the [LeechCore wiki](https://github.com/ufrisk/LeechCore/wiki).

#### Examples:
Analyze the physical memory dump memorydump.raw:<br>
`-device memorydump.raw`

Analyze live memory acquired with a PCILeech FPGA device connected over USB (required additional DLLs - check [LeechCore wiki](https://github.com/ufrisk/LeechCore/wiki/Device_FPGA) for additional details):<br>
`-device fpga`

Analyze live memory acquired in real-time with WinPMEM (requires MemProcFS to be started as elevated administrator, the WinPMEM driver _att_winpmem_64.sys_ to be downloaded):<br>
`-device "pmem://att_winpmem_64.sys"`

<hr>

### `-remote`
MemProcFS may connect to a remote instance in the form of a running LeechAgent by specifying the `-remote` option. The remote LeechAgent may use any of the supported memory acquisition devices. Default is to run memory analysis on the local machine - acquiring memory from the remote machine over the network. It's also possible to run memory analysis on the remote machine (recommended) with additional command line option `-remotefs`. For more information about the option consult the MemProcFS remoting wiki entry](https://github.com/ufrisk/MemProcFS/wiki/_Remoting).

#### Examples:
Connect to a remote LeechAgent running on the host ad-test.ad.example.org over an insecure unauthenticated cleartext RPC connection - port tcp/28473:<br>
`-remote rpc://insecure:ad-test.ad.example.org`

Connect to a remote LeechAgent running on the host ad-test.ad.example.org over an active-directory kerberos-secured mutually authenticated encrypted connection tunneled over SMB/445:<br>
`-remote smb://ad-test$@AD.EXAMPLE.ORG:ad-test.ad.example.org`

Connect to a remote LeechAgent running on the host ad-test.ad.example.org over using NTLM as the authentication method. Set up an encrypted connection tunneled over SMB/445:<br>
`-remote smb://ntlm:ad-test.ad.example.org`

<hr>

### `-remotefs`
Used in conjunction with the `-remote` command line option (see above). Force the memory analysis to be performed on the remote computer instead of on the local computer. Files are retrieved from the remote computer.

#### Examples:
Connect to a remote LeechAgent, running MemProcFS on the remote host, on the host ad-test.ad.example.org over an insecure unauthenticated cleartext RPC connection - port tcp/28473:<br>
`-remote rpc://insecure:ad-test.ad.example.org` -remotefs

Connect to a remote LeechAgent, running MemProcFS on the remote host, running on the host ad-test.ad.example.org over an active-directory kerberos-secured mutually authenticated encrypted connection tunneled over SMB/445:<br>
`-remote smb://ad-test$@AD.EXAMPLE.ORG:ad-test.ad.example.org` -remotefs

<hr>

### `-max`
Maximum allowed physical memory address to read from. This option should normally not be used. It's mostly available for debug purposes. If a low value is specified MemProcFS is unlikely to be able to start or analyze memory properly. If this option is not specified the maximum amount of physical memory will be auto-detected (recommended).

#### Example:
Set the maximum allowed physical memory address to 4GB (0x100000000):<br>
`-max 0x100000000`

## The `py` per-process directory

The directory _py_ exists as a sub-directory in each process directory if Python support has been loaded. The directory is a placeholder sub-directory for various process-related plugins coded in Python.

### For Developers
The _py_ sub-directory exists only if Python plugin support has been loaded. The directory is not a plugin in itself - but it contains per-process plugins coded in Python. For more information about Python modules check out the [Python Plugins](https://github.com/ufrisk/MemProcFS/wiki/Dev_Python) wiki topic.

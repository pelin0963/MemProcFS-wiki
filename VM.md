## The `vm` virtual machine directory

The directory _vm_ exists as a sub-directory to the file system root.

The _vm_ directory is hidden by default. It will appear if supported virtual machines are detected. VM detection must be enabled by command-line argument.

#### Prior art - Hyper-V Internals and LiveCloudKd:
The MemProcFS virtual machine (VM) detection and parsing is largely based on the works of [@gerhart_x](https://twitter.com/gerhart_x) and his most awesome research presented on his blog [Hyper-V Internals](http://hvinternals.blogspot.com/). The MemProcFS expands on this work by also supporting analysis of memory dump files. For live introspection of Hyper-V virtual machines [LiveCloudKd](https://github.com/gerhart01/LiveCloudKd) and its [MemProcFS plugin](https://github.com/ufrisk/LeechCore/wiki/Device_LiveCloudKd) is recommended since it will also support writing to guest memory and additional types of virtual machines.

MemProcFS also support access to live [VMWare](https://github.com/ufrisk/LeechCore/wiki/Device_VMWare) virtual machines. MemProcFS is currently not able to parse VMWare from memory dump files.

## MemProcFS Virtual Machine support:
MemProcFS support parsing of Hyper-V machines natively. VM parsing is only supported on 64-bit MemProcFS builds.

Virtual machines may be parsed from live memory (acquired with PCILeech DMA or memory acquisition drivers such as WinPMEM) or from memory dump files from a host operating system with active VMs.

Parsing virtual machines will allow for separate memory forensics of the virtual machines.

The MemProcFS virtual machine parsing should at the moment be considered as experimental. Please report any issues.

#### Supported VMs:
* Full Hyper-V virtual machines.
* Hyper-V containers and Sandboxes.
* WSL2 virtual machines.
* Windows Hypervisor Platform VMs (VMWare/VirtualBox on Hyper-V).

#### Unsupported VMs:
* Virtual machines on host operating systems older than Windows 10 - i.e. Windows 8, Server2012R2.
* The Windows Secure Kernel including Credential Guard.
* Virtual machines on 32-bit operating systems.
* Non-Hyper-V hypervisors such as native VMWare/VirtualBox.

#### Command-line arguments to enable VM detection:

Virtual machine detection is not enabled by default. Virtual machine detection must be enabled with a command-line argument at startup.

| Command-line          | Description |
|-----------------------|-------------|
| `-vm `                | Detect and parse virtual machines and mount them under /vm. Windows virtual machines will be mounted as a full MemProcFS virtual file system. |
| `-vm-basic`           | Detect and parse virtual machines and mount them under /vm. Only the virtual machine physical memory will be exposed. |
| `-vm-nested`          | Detect and parse virtual machines and mount them under /vm. Windows virtual machines will be mounted as a full MemProcFS virtual file system. Also detect additional virtual machines inside the virtual machines. |
| `-forensic [1-4]`     | Forensic mode will automatically enable virtual machine detection similar to the `-vm` option. It is possible to combine forensic mode with either of the above options.

Files in the _vm/[subvm]_ directories are read/write as follows by the root MemProcFS instance.



### Example

The example shows browsing the sys folder and the computer name of the VM named _T-WIN10-X64-1909-18363_, as well as listings info file _/vm/vm.txt_.

[[resources/root_vm.png]]

### For Developers
The _vm_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_vm.c_ in the vmm project. Substantial VM functionality is part of the MemProcFS core.

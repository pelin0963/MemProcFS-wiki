## The `heaps` per-process directory

The directory _heaps_ exists as a sub-directory in each process directory.

The _heaps_ directory contains one sub-directory for each heap as well as info files about the heaps.

Each module directory contains a number of informational files and directories related to the loaded module. The files and directories and their contents are listed below:

| File               | Description                                                                   |
| ------------------ | ----------------------------------------------------------------------------- |
| __readme.txt__     | Readme with information about the heap parsing.                               |
| __heaps.txt__      | Information about the process heaps.                                          |
| __segments.txt__   | Information about the segments (memory ranges) related to the heaps.          |
| [heap-id]/__allocations.txt__    | Heap allocations for the specified heap.                        |
| [heap-id]/__allocations-v.txt__  | Heap allocations and the first 16 bytes for each allocation.    |
| [heap-id]/[num]/__[addr].mem__   | Heap allocation memory                                          |

The heap module parses NT heaps as well as segment heaps (on Windows 10 1809/17763 and later).

Text files are read only. .mem files in sub-directories are writable if a write-capable memory acquisition device is used.



### File: heaps.txt

The file _heaps.txt_ contains information about heaps related to the process. The meaning of the different columns are as follows:

```
   #    PID Heap          Address Type   
--------------------------------------   
0000   3668    1 0000000000c90000 Nt
0001   3668    0 00000000011e0000 Nt
0002   3668    2 0000000001380000 Nt
0003   3668    3 0000000002cb0000 Nt
0004   3668   60 00000000032e0000 Nt
```


### File: segments.txt

The file _segments.txt_ contains information about the heap segments (memory ranges). The meaning of the different columns are as follows:

```
   #    PID Heap          Address Type      
--------------------------------------      
0000   3668    1 0000000000c90000 NtSegment
0001   3668    0 0000000000dd0000 NtLfh
0002   3668    2 0000000001170000 NtLfh
0003   3668    0 00000000011e0000 NtSegment 
0004   3668    2 0000000001380000 NtSegment
0005   3668    3 0000000002cb0000 NtSegment
0006   3668   59 0000000003000000 NtLfh
0007   3668   60 00000000032e0000 NtSegment
```



### File: allocations-v.txt

The file _allocations-v.txt_ contains information about heap allocations related to a specific heap. HexAscii16 in the -v file contains the first 16 bytes of each heap allocation. The meaning of the different columns are as follows:

```
   #    PID Heap          Address     Size Type     HexAscii16                                                         
--------------------------------------------------------------                                                         
0000   3668    0 00000000011e0750       2c Nt       43 00 3a 00 5c 00 57 00  69 00 6e 00 64 00 6f 00   C.:.\.W.i.n.d.o.
0001   3668    0 00000000011e07a0       52 Nt       43 00 3a 00 5c 00 57 00  69 00 6e 00 64 00 6f 00   C.:.\.W.i.n.d.o.
0002   3668    0 00000000011e0810       38 Nt       80 74 1e 01 00 00 00 00  88 33 9f e2 fa 7f 00 00   .t.......3... ..
0003   3668    0 00000000011e0860       f0 Nt       10 20 20 01 00 00 00 00  f0 84 1e 01 00 00 00 00   .  .............
0004   3668    0 00000000011e0970      1c8 Nt       02 00 00 00 00 00 00 00  c0 b1 9a e2 fa 7f 00 00   ............. ..
0005   3668    0 00000000011e0b50      1c8 Nt       c4 01 00 00 00 00 00 00  00 00 00 00 00 00 00 00   ................
0006   3668    0 00000000011e0d30       38 Nt       30 0d 1e 01 00 00 00 00  30 0d 1e 01 00 00 00 00   0.......0.......
0007   3668    0 00000000011e0dc0       f0 Nt       80 16 20 01 00 00 00 00  60 31 6f 04 00 00 00 00   .. .....`1o.....
```



### Example

The example below shows the files in the _heaps/0_ sub-directory of the _cmd.exe_ process where 0 is the heap id. It shows an overview of verbose allocations in heap 0 as well as an overview of the process heaps.

[[resources/proc_heap.png]]

### For Developers
The _heaps_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_proc_heap.c_ in the _vmm_ project.
## The `sys/memory` directory

The directory _sys/memory_ exists as a sub-directory to the file system root.

The directory contains information related to physical memory and its usage.

The files in the **sys/memory** directory are listed in the table below:
| File                  | Description                                               |
| ----------------------| --------------------------------------------------------- |
| memory/pfndbaddr.txt  | virtual address of the PFN database.                      |
| memory/pfndb.txt      | short information about each PFN.                         |
| memory/physmemmap.txt | physical memory map as interpreted by Windows registry.   |

Files in the _sys/memory_ directory are read-only.


### File: pfndb.txt

The file _pfndb.txt_ contains information from the Windows page frame number (PFN) database. Each PFN denotes one 0x1000 (4096) byte physical memory page. For memory mapped as process private the process PID is shown. The flags are as following: [Priority 0-7][Modified M/-][Prototype P/-]. Also the corresponding virtual address is shown if it can be derived.
```
PFN#     PID TYPE    SUBTYPE    FLAG  VIRTUALADDRESS
====================================================
2be54      0 Active  File       7-P                0
2be55      0 Active  File       7-P                0
2be56   3908 Active  ProcPriv   5M-     7ff836d72000
2be57   2828 Active  ProcPriv   5M-      20d24013000
...
```

### Example

The example shows the _sys/memory_ directory with the physical memory map and select entries from the page frame number (PFN) database describing physical memory pages.

[[resources/root_sysinfo_memory.png]]

### For Developers
The _sys/memory_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_mem.c_ in the vmm project.

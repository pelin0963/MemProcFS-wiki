# Developing plugins in Python

**NB! Python plugins are currently only supported on Windows.**

## Light plugins

Light plugins are smaller faster-to-run plugins which is utilizing the VmmPy API.

Light plugins are easy-to-construct and print its output to stdout (with Python print() function). They show up in sub-directories of the `/py/` root directory. They may be constructed on a global basis or per-user basis, not on a per-process basis.

It's important that light plugins are fairly quick to render and that output isn't too large since it will be cached in-memory.

Light plugins are placed in the plugin directory and should be named according to the following convention: `pyp_<ignored>_<root|user>_<subdir>_<filename>.py`
where:
  * `<ignored>` is ignored.
  * `<root|user>` should be either of root (standard global plugin) or user (by-user plugin). NB! it's not possible to have per-process light plugins.
  * `<subdir>` is the sub-directory, for nested sub-directories use $ i.e. dir1$dir2 -> dir1/dir2.
  * `<filename>` is the file name without the standard .txt file extension.

Each light plugin is treated as a standalone python file. They however have access to the following built-in objects:
  * `vmm` - the memprocfs root object (see Python API documentation).
  * `path` - the plugin path in the virtual file system as string.
  * `user` - user information as a dict.

An example is: `pyp_reg_root_reg$usb_usb$storage.py`. More examples of light plugins may be found in the MemProcFS plugins directory.


## Standard plugins

This section relates to integration python plugin functionality in MemProcFS - for including MemProcFS in stand alone applications and scripts please check out the [Python API](./API_Python) section.

Python plugin functionality requires that MemProcFS is able to access a Python 3.6 (or later) installation - either an embedded Python in the `python` sub-directory or on the system path.

Creating a minimal plugin is as simple as dropping a python module called `pym_*` in the plugins directory. Please see the example Python plugin/module _pym_procstruct_ for how to do this.

The Python plugin module should make the `Initialize` and `Close` functions available in its __init__.py file - like in the example below:

**__init__.py**:
```Python
from plugins.pym_testmodule.pym_testmodule import (
    Initialize,
    Close,
)
__all__ = [
    "Initialize",
    "Close",
]
```

**pym_testmodule.py**:
```Python
import memprocfs
from vmmpyplugin import *

def Callback_List(pid, path):
    # not part of example - please see pym_procstruct.py for example info.
    pass

def Callback_Read(pid, path, bytes_length, bytes_offset):
    # not part of example - please see pym_procstruct.py for example info.
    pass

def Callback_Write(pid, path, bytes_data, bytes_offset):
    # not part of example - please see pym_procstruct.py for example info.
    pass

def Close():
    # not part of example - please see pym_procstruct.py for example info.
    pass

def Initialize(target_system, target_memorymodel):
    # Check that the operating system is 32-bit or 64-bit Windows. If it's not
    # then raise an exception to terminate loading of this module.
    if target_system != memprocfs.VMMPY_SYSTEM_WINDOWS_X64 and target_system != memprocfs.VMMPY_SYSTEM_WINDOWS_X86:
        raise RuntimeError("Only Windows is supported by the pym_procstruct module.")
    # Register a directory with the VmmPyPlugin plugin manager. The directory
    # is a non-root (i.e. a process) directory and have a custom List function.
    VmmPyPlugin_FileRegisterDirectory(False, 'procstruct', Callback_List)
    # alternatively: register a file (in or not in a sub-directory to /py/) with the
    # python plugin manager. Necessary sub-directories will be created if required.
    VmmPyPlugin_FileRegister(False, 'testdir/testfile.txt', 4096, Callback_Read, Callback_Write)
```

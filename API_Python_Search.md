## Overview:

The MemProcFS search API for Python consists of two primary objects:

* [`VmmSearch`](#VmmSearch) - binary search object.
* [`VmmYara`](#VmmYara) - YARA search object.

Searches can be performed either asynchronously (non-blocking) or synchronously (blocking).

For additional information about how searches may be conducted see the [examples](https://github.com/ufrisk/MemProcFS/blob/master/files/memprocfs_example.py).

### synchronous search:
1. Instantiate the search object (`VmmSearch` or `VmmYara`) using one of the sources.
2. Perform additional configuration of the search object.<br/> This is optional for the YARA search.<br/> For the binary search at least one search term must be added with `.add_search()`.
3. Start the search and retrieve the search results with a call to `.result()`. It may take some time for the search to complete.

### asynchronous search:
1. Instantiate the search object (`VmmSearch` or `VmmYara`) using one of the sources.
2. Perform additional configuration of the search object.<br/> This is optional for the YARA search.<br/> For the binary search at least one search term must be added with `.add_search()`.
3. Start the search in the background with a call to `.start()`.
4. Monitor the progress of the search by reading `.is_completed` or `.addr_current`.<br/> Optionally abort the search by calling `.abort()`.<br/> Poll the partial result with calls to `.poll()`
5. When the search is completed - retrieve the final results with a call to `.result()`. If search is not yet completed this function will block until the search is completed (or aborted).



## VmmSearch:

Represents a binary search object.

### Example:
The example shows a search for the PE MZ header in physical address space. Only the first 3 matches are shown.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/api_py_vmmsearch.png"/>

### Sources:
* `vmm.search()` - search physical memory.
* `process.search()` - search process virtual memory.

### Attributes:
```Python
search.addr_current    # int: current address being searched. ex: search.addr_current -> 65535
search.bytes_searched  # int: number of bytes searched so far. ex: search.bytes_searched -> 402673664
search.is_aborted      # bool: search is aborted. ex: search.is_aborted -> False
search.is_completed    # bool: search is completed. ex: search.is_completed -> True
search.is_completed_success # bool: search is successfully completed. ex: search.is_completed_success -> True
search.is_started      # bool: search is started. ex: search.is_started -> True
search.num_searches    # int: number of search terms added with add_search. ex: search.num_searches -> 1
search.pid             # int: process id (if virtual memory search). ex: search.pid -> 4
search.addr_max        # int: max address to search. ex: search.addr_max -> 140737488355327
search.addr_min        # int: min address to search. ex: search.addr_min -> 0
search.flags           # int: combination of one or multiple memprocfs.FLAGS_*. ex: search.flags -> 1
```

### Methods:
```Python
# Add a search term.
# Up to 16 search terms may be added.
# -- search = bytes: search term (max 32 bytes).
# -- mask = opt bytes: wildcard mask (bit 1 = wildcard bit) no longer than search. Or None.
# -- alignment = opt int: search alignment in bytes from page start, 1, 2, 4, 8, 16 .. 4096.
# -- return = int: the id of the search term.
search.add_search(bytes: search, opt bytes: mask, opt int: alignment) # -> int
# examples:
#   search.add_search(b'MZ') -> 0
#   search.add_search(b'MZ', None, 0x1000) -> 1

# Abort an ongoing search.
search.abort()
# example:
#   search.abort()

# Start searching asynchronously in the background.
search.start()
# example:
#   search.start()

# Poll an ongoing search and retrieve any search matches already found (non-blocking).
# -- return = list: list of matches. A match consists of the address and the search term id.
search.poll() # -> list
# example:
#   search.poll() ->
#   [[104648704, 0], [8791713890304, 0], ...]

# Retrieve all search results from a search (blocking).
# -- return = list: list of matches. A match consists of the address and the search term id.
search.result() # -> list
# example:
#   search.result() ->
#   [[104648704, 0], [8791713890304, 0], ...]
```



## VmmYara:

Represents a YARA search object.

### Example:
The example shows a search for the PE MZ header in process virtual address space. Only the first 2 matches are shown.
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/api_py_vmmyara.png"/>

### Sources:
* `vmm.search_yara()` - search physical memory.
* `process.search_yara()` - search process virtual memory.

### Attributes:
```Python
yara.addr_current    # int: current address being searched. ex: yara.addr_current -> 65535
yara.bytes_searched  # int: number of bytes searched so far. ex: yara.bytes_searched -> 402673664
yara.is_aborted      # bool: search is aborted. ex: yara.is_aborted -> False
yara.is_completed    # bool: search is completed. ex: yara.is_completed -> True
yara.is_completed_success # bool: search is successfully completed. ex: yara.is_completed_success -> True
yara.is_started      # bool: search is started. ex: yara.is_started -> True
yara.max_results     # int: maximum number of search results. ex: yara.max_results -> 65536
yara.pid             # int: process id (if virtual memory search). ex: yara.pid -> 4
yara.addr_max        # int: max address to search. ex: yara.addr_max -> 140737488355327
yara.addr_min        # int: min address to search. ex: yara.addr_min -> 0
yara.flags           # int: combination of one or multiple memprocfs.FLAGS_*. ex: yara.flags -> 1
```

### Methods:
```Python
# Abort an ongoing search.
yara.abort()
# example:
#   yara.abort()

# Start searching asynchronously in the background.
yara.start()
# example:
#   yara.start()

# Poll an ongoing search and retrieve any search matches already found (non-blocking).
# -- return = list: list of matches. A match consists of the address and the search term id.
yara.poll() # -> list
# example:
#   yara.poll() ->
#   [{'id': 'mz_header', 'tags': [], 'meta': [], 'matches': {'MZ': [33816576]}}, ...]

# Retrieve all search results from a search (blocking).
# -- return = list: list of matches. A match consists of the address and the search term id.
yara.result() # -> list
# example:
#   yara.result() ->
#   [{'id': 'mz_header', 'tags': [], 'meta': [], 'matches': {'MZ': [33816576]}}, ...]
```
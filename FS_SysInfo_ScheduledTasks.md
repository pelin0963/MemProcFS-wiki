## The `sys/tasks` directory

The directory _sys/tasks_ exists as a sub-directory to the file system root.

The directory and its sub-directories contains information about scheduled tasks extracted from the registry.

The files in the **sys/tasks** directory are listed in the table below:
| File                            | Description                                               |
| ------------------------------- | --------------------------------------------------------- |
| __tasks.txt__                   | Summary information about all scheduled tasks. |
| by-guid/[guid]/registry/        | Service registry key.                                     |
| by-guid/[guid]/__taskinfo.txt__ | Detailed information about each scheduled task.                  |
| by-name/[name]/registry/        | Service registry key.                                     |
| by-name/[name]/__taskinfo.txt__ | Detailed information about each scheduled task.                  |

Files in the _sys/tasks_ directory and sub-directories are read-only except for binary registry key/values.

### File: tasks.txt

The file _tasks.txt_ contains summary information about the scheduled tasks. The meaning of the different columns are as follows:

```
   # Task GUID                              Task Name                            Time (Most Recent)      User     Command Line :: Parameters
--------------------------------------------------------------------------------------------------------------------------------------------
0000 {E1C54205-C98A-4568-99F5-F069A71C9733} \CreateExplorerShellUnelevatedTask   2019-03-07 15:09:07 UTC Author   C:\Windows\Explorer.EXE :: /NOUACCHECK
0001 {B313253F-006F-4A3C-88E6-09226D66275B} \DolbySelectorTask                   2019-03-29 09:41:11 UTC Author   %ProgramFiles%\Dolby Digital Plus\ddp.exe :: -autostart
0002 {C78F9350-0638-4EEC-8232-02454A55F44A} \DropboxUpdateTaskMachineCore        2019-03-29 14:18:00 UTC Author   C:\Program Files (x86)\Dropbox\Update\DropboxUpdate.exe :: /c
...
```

### File: by-guid/[guid]/taskinfo.txt &nbsp;&nbsp;and&nbsp;&nbsp; by-name/[name]/taskinfo.txt

The file _taskinfo.txt_ contains detailed information about each scheduled task as shown below:

```
Name:       Diagnostics
Path:       \Microsoft\Windows\DiskFootprint\Diagnostics
User:       System
Command:    %windir%\system32\disksnapshot.exe
Parameters: -z
GUID:       {240478A4-B7D2-43B1-AF21-626C77E72C1F}
RegLastWr:  2017-03-18 21:06:25 UTC
Create:     2018-10-11 13:50:43 UTC
LastRun:    2019-03-28 15:09:04 UTC
Completed:  2019-03-28 15:09:05 UTC
```

### Example

The example shows the _sys/tasks_ directory with the summary information and detailed information about one scheduled task.

[[resources/root_sysinfo_tasks.png]]

### For Developers
The _sys/tasks_ sub-directory is implemented as a built-in native C-code plugin. The plugin source is located in the file _modules/m_sys_task.c_ in the vmm project.

## Overview:

The MemProcFS registry API for Python consists of the following objects:

* [`VmmRegHive`](#VmmRegHive) - registry hive object.
* [`VmmRegKey`](#VmmRegKey) - registry key object.
* [`VmmRegValue`](#VmmRegValue) - registry value object.
* [`memprocfs.RegUtil`](#RegUtil) - registry utility functions.

Registry Hives, Keys and Values are retrieved from the base [`Vmm`](Dev_Base#Vmm) object or from eachother.


## VmmRegHive:

Represents a registry hive.

### Example:
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/api_py_vmmreghive.png"/>

### Sources:
* `vmm.reg_hive_list()` - as list of all hives.

### Attributes:
```Python
reghive.name           # str: hive name. ex: reghive.name -> '0xffffbc079e436000-settingsdat-A_{c4fac4f4-b28f-17e2-b8a7-4d3640adf5aa}'
reghive.name_short     # str: short hive name. ex: reghive.name_short -> '1h2txyewy\\Settings\\settings.dat'
reghive.path           # str: hive kernel object path. ex: reghive.path -> '\\REGISTRY\\A\\{c4fac4f4-b28f-17e2-b8a7-4d3640adf5aa}'
reghive.size           # int: hive size of on-disk hive. ex: reghive.size -> 12288
reghive.addr           # int: address of CMHIVE object. ex: reghive.addr -> 18446669339638849536
reghive.addr_baseblock # int: address of HIVE BASE BLOCK (.regf). ex: reghive.addr_baseblock -> 18446669339638857728
reghive.rootkey        # VmmRegKey: root key of the hive. ex: reghive.rootkey -> RegKey:ROOT
reghive.orphankey      # VmmRegKey: orphan 'virtual' root key of the hive. ex: reghive.orphankey -> RegKey:ORPHAN
reghive.memory         # VmmRegMemory: registry memory functions. Please see methods below for more info.
```

### Methods:
```Python
# read raw data from the registry hive address space.
# The non-volatile address space ranges from 0..size.
# The volatile address space range from 0x80000000..N.
# -- hive_addr = the hive address to read from
# -- length = number of bytes to read
# -- flags = optional read flags memprocfs.FLAG_*
# -- return = object
reghive.memory.read(int: hive_addr, int: length, opt int: flags) # -> bytes


# write raw data from the registry hive address space.
# The non-volatile address space ranges from 0..size.
# The volatile address space range from 0x80000000..N.
# NB! not recommended! high probability to corrupt hive!
# -- hive_addr = the hive address to read from
# -- data = data to write.
reghive.memory.write(int: hive_addr, bytes: data) # -> None
```



## VmmRegKey:

Represents a registry key.

### Example:
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/api_py_vmmregkey.png"/>

### Sources:
* `vmm.reg_key()`
* `reghive.rootkey`
* `reghive.orphankey`
* `regkey.parent`
* `regkey.subkeys()` = as list of subkeys.
* `regkey.subkeys_dict()` = as dict of subkeys keyed by name.
* `regvalue.parent`

### Attributes:
```Python
regkey.name     # str: key name. ex: regkey.name -> 'Run'
regkey.path     # str: key path. ex: regkey.path -> 'HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run'
regkey.parent   # VmmRegKey: parent registry key. ex: regkey.parent -> RegKey:CurrentVersion
regkey.time_int # int: key last write time. ex: regkey.time_int -> 131983260431204405
regkey.time_str # str: key last write time. ex: regkey.time_str -> '2019-03-29 09:40:43 UTC'
```

### Methods:
```Python
# Retrieve the subkeys belonging to this registry key.
# -- return = list of VmmRegKey
regkey.subkeys() # -> list:VmmRegKey


# Retrieve the subkeys belonging to this registry key.
# -- return = dict of VmmRegKey keyed by subkey name.
regkey.subkeys_dict() # -> dict:VmmRegKey


# Retrieve the values belonging to this registry key.
# -- return = list of VmmRegValue
regkey.values() # -> list:VmmRegValue


# Retrieve the subkeys belonging to this registry key.
# -- return = dict of VmmRegValue keyed by value name.
regkey.values_dict() # -> dict:VmmRegValue
```



## VmmRegValue:

Represents a registry key value.

### Example:
<img src="https://github.com/ufrisk/MemProcFS/wiki/resources/api_py_vmmregvalue.png"/>

### Sources:
* `vmm.reg_value()`
* `regkey.values()` = as list of values.
* `regkey.values_dict()` = as dict of values keyed by name.

### Attributes:
```Python
regvalue.name   # str: value name. ex: regvalue.name -> 'SecurityHealth'
regvalue.path   # str: value path. ex: regvalue.path -> 'HKLM\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\SecurityHealth'
regvalue.parent # VmmRegKey: parent registry key. ex: regvalue.parent -> RegKey:Run
regvalue.size   # int: value byte size. ex: regvalue.size -> 90
regvalue.type   # int: value type as memprocfs.WINREG_*. ex: regvalue.type -> 2
regvalue.value  # bytes: the raw registry value as a bytes object. ex: regvalue.value -> b'%\x00P\x00r\x00o\x00g\x00r\x00a\x00m\x00F ...'
```

### Methods:
```Python
# Retrieve the value as DWORD.
# -- is_typecheck = optional flag to disable type check.
# -- return = int
regvalue.vdword(opt bool: is_typecheck) # -> int


# Retrieve the value as QWORD.
# -- is_typecheck = optional flag to disable type check.
# -- return = int
regvalue.vqword(opt bool: is_typecheck) # -> int


# Retrieve the QWORD value as time string.
# -- is_typecheck = optional flag to disable type check.
# -- return = int
regvalue.vtime(opt bool: is_typecheck) # -> str


# Retrieve the value as text. This retrieves text as UTF16-LE which
# is the most commonly format used in the Windows registry.
# -- is_typecheck = optional flag to disable type check.
# -- return = str
regvalue.vstr(opt bool: is_typecheck) # -> str


# Retrieve the value as ascii text. This is rarely used.
# -- return = str
regvalue.vascii() # -> str

```


## RegUtil:

`memprocfs.RegUtil` is a utility object with static methods exposing registry access helper functionality.

Please see MemProcFS `plugins/pyp_reg_*.py` plugins for example usage.

### Methods:
```Python
# Convert a Windows FileTime integer to string.
# -- ft_int = int: Windows FileTime value.
# -- return = str: 23 char time in format: '%Y-%m-%d %H:%M:%S UTC' / '2020-01-01 23:59:59 UTC'.
memprocfs.ft2str(ft_int) # -> str


# Print a key-value pair on the screen with formatting
# -- indent_int = int: indent in # spaces.
# -- key_str = str: key name.
# -- value_str = str: optional value string; default: ''.
# -- line_length = int: optional line length; default: 80.
# -- is_line_truncate = bool: optional truncate flag; default False.
# -- is_value_bracked = bool: optional put value str inside []; default False.
memprocfs.print_keyvalue(indent_int, key_str, value_str = '', line_length = 80, is_line_truncate = False, is_value_bracket = False)


# Print a keyname/filetime pair on the screen with formatting.
# -- indent_int = int: indent in # spaces.
# -- key_str = str: key name.
# -- ft_int = int: time in Windows FILETIME format.
# -- line_length = int: optional line length; default: 80.
# -- is_line_truncate = bool: optional truncate flag; default False.
# -- is_value_bracked = bool: optional put value str inside []; default False.
memprocfs.print_filetime(indent_int, key_str, ft_int, line_length = 80, is_line_truncate = False, is_value_bracket = False)


# Read a UTF-16 value from registry with exceptions suppressed.
# -- vmm = Vmm: MemProcFS VMM object.
# -- reg_value_path = str: value path/name __OR__ bytes: raw value.
# -- is_skip_typecheck = bool: skip typecheck (REG_SZ, REG_EXPAND_SZ); default False.
# -- return = str: value on success, '' on fail.
memprocfs.read_utf16(vmm, reg_value_path, is_skip_typecheck = False) # -> str


# Read an ascii value from registry with exceptions suppressed.
# -- vmm = Vmm: MemProcFS VMM object.
# -- reg_value_path = str: value path/name __OR__ bytes: raw value.
# -- return = str: value on success, '' on fail.
memprocfs.read_ascii(vmm, reg_value_path) # -> str


# Read a 64-bit 'QWORD' value from registry with exceptions suppressed.
# -- vmm = Vmm: MemProcFS VMM object.
# -- reg_value_path = str: value path/name __OR__ bytes: raw value.
# -- is_skip_typecheck = bool: skip typecheck; default False.
# -- return = int: value on success, -1 on fail.
memprocfs.read_qword(vmm, reg_value_path, is_skip_typecheck = False) # -> int


# Read a 32-bit 'DWORD' value from registry with exceptions suppressed.
# -- vmm = Vmm: MemProcFS VMM object.
# -- reg_value_path = str: value path/name __OR__ bytes: raw value.
# -- is_skip_typecheck = bool: skip typecheck; default False.
# -- return = int: value on success, -1 on fail.
memprocfs.read_dword(vmm, reg_value_path, is_skip_typecheck = False) # -> int


# Convert a MRUListEx reg value into a list.
# -- mrulistex_bytes = bytes: value of MRUListEx data.
# -- return = array: int of value MRUListEx values.
memprocfs.mrulistex_expand(mrulistex_bytes) -> list
```